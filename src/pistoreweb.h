/*******************************************************************************
 * pistoreweb - a RESTful web server based on Mongoose.
 *
 * pistoreweb.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PISTOREWEB_H
#define PISTOREWEB_H

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef PISTOREWEB_C
#endif /* PISTOREWEB_C */

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "pistoreweb"
#define MAXBUF      4096
#define HTTP_PORT   80
#define HTTP_PORT_T 80
#define API_VERSION "1.0"

#define KB      (1024)
#define MB      (1024*1024)
#define GB      (1024*1024*1024)

#define S_KB        "KB"
#define S_MB        "MB"
#define S_GB        "GB"

// Where the config file is located
#define F_CFG   "/etc/pistoreweb.cfg"
#define F_CFG_T "data/pistoreweb.cfg"

// Web directories and configuration
#define WEBROOT     "/home/httpd/pistoreweb"
#define DATADIR     "/etc"
#define INDEXPG     "index.tmpl"
#define F_404       "404.html"

#define PAIR_IOT     "/pair/iot"
#define PAIR_JARVIS  "/pair/jarvis"

/* Enable AES 128 CBC encryption */
#define CBC 1

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef PISTOREWEB_C
#endif

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include <pibox/parson.h>
#include "mongoose.h"
#include "cli.h"
#include "utils.h"
#include "msgQueue.h"
#include "init.h"
#include "base64.h"
#include "aes.h"
#include "pkcs7_padding.h"
#include "crypto.h"
#include "get.h"
#include "delete.h"
#include "xpost.h"
#include "registration.h"
#include "db.h"

#endif /* !PISTOREWEB_H */
