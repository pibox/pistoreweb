/*******************************************************************************
 * pistoreweb - a UI and RESTful server based on Mongoose
 *
 * pistoreweb.c:  program main
 *
 * PiStore is provides access to storage devices mounted via USB.
 * PiStoreWeb provides an API for managing that data and a UI for browsing it.
 *
 * Terminology
 * ------------------------------------------------------------------------
 * monitor:     Application running on RPi/PiBox used to manage IoT nodes.
 * IoT node:    Remote node, in this case PiStore.
 *
 * The format for REST requests is
 *
 * GET  /api/<version>/status            From monitor             Retrieve total storage under management
 * GET  /api/<version>/identify          From monitor             Toggle identify mechanism.
 * GET  /<path>                          From monitor or browser  Get a directory listing, in HTML.
 * GET  /register?uuid=<uuid>            From monitor             Registration response from monitor.
 * GET  /                                From monitor or browser  Generate HTML showing system status
 * POST /api/<version>/nodename          From monitor or browser  Set visual nodename for PiStore device.
 * POST /api/<version>/pw                From monitor or browser  Set login password.
 * DELETE /api/<version>/file</verified> From monitor or browser  Delete a file
 * DELETE /api/<version>/dir</verified>  From monitor or browser  Delete a directory
 *
 * Version is added to header, as such:
 * Accept-Version: 1.0
 *
 * Additionally, JSON data may be included.  This would be data
 * specific to the cmd.
 *
 * License: 0BSD
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PISTOREWEB_C
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <curl/curl.h>
#include <pibox/pibox.h>
#include <pibox/utils.h>

#include "pistoreweb.h"

static int s_signo = 0;

static void 
signal_handler(int signo)
{
    s_signo = signo;
}

#if 0
/*
 *========================================================================
 * Name:   apiVersionMatch
 * Prototype:  char *apiVersionMatch( char *apiVersion )
 *
 * Description:
 * Tests if the specified version matches what we support.
 *
 * Returns:
 * 0 if the API version is supported.
 * 1 if the API version is not supported.
 *========================================================================
 */
static int
apiVersionMatch( char *apiVersion )
{
    return (strcmp(apiVersion, VERSTR) == 0)? 0 : 1;
}

/*
 *========================================================================
 * Name:   setDefaultHeaders
 * Prototype:  char *setDefaultHeaders( void )
 *
 * Description:
 * Set common headers for outbound messages.
 *
 * Notes:
 * Uses:
 * mg_http_reply(struct mg_connection *c, int status_code, const char *headers, const char *body_fmt, ...);
 * Where headers is a collection of "name: value\n\r" strings.
 * So this function just builds that string and returns it to the caller.
 *
 * Caller is responsible for freeing the returned string.
 *========================================================================
 */
static char *
setDefaultHeaders( void )
{
    char *buf;

    // Set the Accept-Version header.
    buf = calloc(1,strlen("Accept-Version: ")+strlen(VERSTR)+3);
    sprintf(buf, "Accept-Version: %s\r\n", VERSTR);
    return(buf);
}
#endif

/*
 *========================================================================
 * Name:   _showFS
 * Prototype:  static void _showFS( gpointer item, gpointer user_data )
 *
 * Description:
 * Show filesystem data.
 *========================================================================
 */
static void
_showFS( gpointer item, gpointer user_data )
{
    int   i;
    char *str;
    char *fields[7];
    char *asizetype="none", *tsizetype="none";
    unsigned long long total, avail, bsize;
    unsigned long long ctotal, cavail;

    str = strdup( (char *)item );
    fields[0] = strtok(str, ":");
    for (i=1; i<7; i++)
    {
        fields[i] = strtok(NULL, ":");
    }

    total = strtoull(fields[1], NULL, 10);
    avail = strtoull(fields[2], NULL, 10);
    bsize = strtoull(fields[3], NULL, 10);

    ctotal = (unsigned long long)(total*bsize/GB);
    if ( ctotal > 0 )
        tsizetype=S_GB;
    else
    {   
        ctotal = (unsigned long long)(total*bsize/MB);
        if ( ctotal > 0 )
            tsizetype=S_MB;
        else
        {   
            ctotal = (unsigned long long)(total*bsize/KB);
            tsizetype=S_KB;
        }
    }

    cavail = (unsigned long long)(avail*bsize/GB);
    if ( cavail > 0 )
        asizetype=S_GB;
    else
    {   
        cavail = (unsigned long long)(avail*bsize/MB);
        if ( cavail > 0 )
            asizetype=S_MB;
        else
        {   
            cavail = (unsigned long long)(avail*bsize/KB);
            asizetype=S_KB;
        }
    }

    piboxLogger(LOG_INFO, "%s: %llu%s Total, %llu%s Available\n", fields[0], ctotal, tsizetype, cavail, asizetype);
    free(str);

#if 0
    snprintf(buf, 1023, "%s:%llu:%llu:%llu:%s:%s:%s", 
            (char *)mntdir,                         /* Mounted filesystem. This is also what we export. */
            (unsigned long long)statfs->f_blocks,   /* Total disk space on store, in blocks. */
            (unsigned long long)statfs->f_bavail,   /* Available disk space on store, in blocks. */
            (unsigned long long)statfs->f_bsize,    /* blocksize. */
            (flags & F_RO_STAMP)?"Y":"N",           /* Consider the mountpoint read only? */
            (flags & F_PS_STAMP)?"Y":"N",           /* Mountpoint reserved for use by a PiSentry system. */
            (label == NULL)?"None":label            /* Partition label. */
            );
#endif
}

/*
 * ========================================================================
 * Name:   timer_genGraph
 * Prototype:  void timer_genGraph( void *data )
 *
 * Description:
 * Timer callback to update collected stats and generate a graph image.
 * ========================================================================
 */
void
timer_genGraph (void *data)
{
    GSList *storeList = NULL;

    /* Update the display. */
    storeList = dbGetStores();
    g_slist_foreach(storeList, _showFS, NULL);
    utilsGenGraph(storeList);
    g_slist_free_full(storeList, utilsFreeStore);
}

/*
 *========================================================================
 * Name:   handleGet
 * Prototype:  void *handleGet( struct mg_connection *c, struct mg_http_message *hm, int env, void *ev_data )
 *
 * Description:
 * Front end to handling GET requests.
 *========================================================================
 */
static void
handleGet( struct mg_connection *c, struct mg_http_message *hm, int ev, void *ev_data)
{
    char    *uri;
    char    *body;
    char    *remoteIP;
    char    templateURI[256];
    guint   len;
    struct mg_http_serve_opts opts = {0};

    uri = (char *)calloc(1, hm->uri.len + 1 );
    sprintf(uri, "%.*s", (int)hm->uri.len, hm->uri.ptr);
    piboxLogger(LOG_INFO, "uri: %s\n", uri);

    if (strncmp(uri, "/register", 9) == 0 )
    {
        /* This is a response to our registration request to the Ironman Monitor. */
        pistoreRegister( c, hm );
        free(uri);
        return;
    }

    body = (char *)calloc(1, hm->body.len + 1 );
    sprintf(body, "%.*s", (int)hm->body.len, hm->body.ptr);
    piboxLogger(LOG_INFO, "body: %s\n", body);

    remoteIP = utilsGetRemoteIP( c );
    piboxLogger(LOG_INFO, "Requesting IP: %s\n", remoteIP);

    sprintf(templateURI, "/api/%s/", API_VERSION);
    piboxLogger(LOG_INFO, "templateURI: %s\n", templateURI);
    len = strlen(templateURI);

    if (strncmp(uri, templateURI, strlen(templateURI)) != 0)
    {
        /* Not API request. */
        if (strncmp(uri, "/register", 9) == 0 )
        {
        }
        else if (mg_http_match_uri(hm, "/"))
        {
            /* This is just an ordinary HTML request and shows dynamically generated status information. */
            pistoreFrontEnd( c, hm, uri );
        }
        else if (strncmp(uri, "/images", 7) == 0 )
        {
            pistoreImages( c, hm, uri );
        }
        else if (strncmp(uri, "/media/usb", 10) == 0 )
        {
            /* This is just an ordinary HTML request and shows dynamically generated directory listing. */
            pistoreDirList( c, hm );
        }
        else
        {
            piboxLogger(LOG_ERROR, "Invalid directory request: %s\n", uri);
            mg_http_serve_file(c, hm, F_404, &opts);
        }
    }
    else if (strcmp((char *)(uri+len), "status") == 0 )
    {
        /* This is an API request, not a UI request. */
        pistoreStatus( c, body );
    }
    else if (strcmp((char *)(uri+len), "identify") == 0 )
    {
        /* This is an API request, not a UI request. */
        pistoreIdentify( c, body );
    }

    free(remoteIP);
    free(body);
    free(uri);
}

/*
 *========================================================================
 * Name:   handlePost
 * Prototype:  void *handlePost( struct mg_http_message *hm )
 *
 * Description:
 * Front end to handling POST requests.
 *========================================================================
 */
static void
handlePost( struct mg_connection *c, struct mg_http_message *hm )
{
    char    *uri;
    char    *body;
    char    *remoteIP;
    char    templateURI[256];
    guint   len;

    uri = (char *)calloc(1, hm->uri.len + 1 );
    sprintf(uri, "%.*s", (int)hm->uri.len, hm->uri.ptr);
    piboxLogger(LOG_INFO, "uri: %s\n", uri);

    body = (char *)calloc(1, hm->body.len + 1 );
    sprintf(body, "%.*s", (int)hm->body.len, hm->body.ptr);
    piboxLogger(LOG_INFO, "body: %s\n", body);

    remoteIP = utilsGetRemoteIP( c );

    sprintf(templateURI, "/api/%s/", API_VERSION);
    piboxLogger(LOG_INFO, "templateURI: %s\n", templateURI);
    if (strncmp(uri, templateURI, strlen(templateURI)) != 0)
    {
        /* Bad prefix/version */
        mg_http_reply(c, 400, NULL, "\n");
    }
    len = strlen(templateURI);

    if (strcmp((char *)(uri+len), "nodename"))
    {
        /* This is an API request, not a UI request. */
        pistoreSetNodename( c, body );
    }
    else if (strcmp((char *)(uri+len), "pw"))
    {
        /* This is an API request, not a UI request. */
        pistoreSetPW( c, body );
    }
    else
    {
        /* All other requests are unauthorized. */
        mg_http_reply(c, 401, NULL, "\n");
    }

    free(remoteIP);
    free(body);
    free(uri);
}

/*
 *========================================================================
 * Name:   handleDelete
 * Prototype:  void *handleDelete( struct mg_http_message *hm )
 *
 * Description:
 * Front end to handling DELETE requests.
 *========================================================================
 */
static void
handleDelete( struct mg_connection *c, struct mg_http_message *hm )
{
    char    *body;
    char    *remoteIP;

    body = (char *)calloc(1, hm->body.len + 1 );
    sprintf(body, "%.*s", (int)hm->body.len, hm->body.ptr);

    remoteIP = utilsGetRemoteIP( c );

    if (mg_http_match_uri(hm, "/api/version/file"))
    {
        /* This is an API request, not a UI request. */
        pistoreDeleteFile( c, body );
    }
    else if (mg_http_match_uri(hm, "/api/version/dir"))
    {
        /* This is an API request, not a UI request. */
        pistoreDeleteDir( c, body );
    }
    else
    {
        /* No other delete requests are supported. */
        mg_http_reply(c, 401, NULL, "\n");
    }

    free(remoteIP);
    free(body);
}

/*
 *========================================================================
 * Name:   eventHandler
 * Prototype:  void eventHandler( void )
 *
 * Description:
 * Handler called by mg_mgr_poll() when inbound messages arrive.
 * This just tests the HTTP method and passes the messages to the appropriate
 * method handlers.
 *
 * Notes:
 * Every inbound HTTP message contains a JSON object with two fields.
 * iv field       - The initializing vector used with AES 128 CBC encryption.
 * message field  - A JSON command object that is AES 128 CBC encypted using the 
 *                  IV in the HTTP message and the UUID of the sending node.
 *
 * The contents of the JSON command object in the message field depends on the 
 * pistoreweb API being processed.  Fields in this JSON can include a UUID of
 * a node, the command to send to the node and a new state for the node.
 *========================================================================
 */
static void
frontEnd(struct mg_connection *c, int ev, void *ev_data)
{
    char    *method;

    if (ev == MG_EV_HTTP_MSG) 
    {
        // The MG_EV_HTTP_MSG event means HTTP request. `hm` holds parsed request,
        // see https://mongoose.ws/documentation/#struct-mg_http_message
        struct mg_http_message *hm = (struct mg_http_message *) ev_data;

        method = (char *)calloc(1, hm->method.len + 1 );
        sprintf(method, "%.*s", (int)hm->method.len, hm->method.ptr);

        piboxLogger(LOG_INFO, "Inbound method: %s\n", method);
        if ( strcmp(method, "GET") == 0 )
            handleGet(c, hm, ev, ev_data);
        else if ( strcmp(method, "POST") == 0 )
            handlePost(c, hm);
        else if ( strcmp(method, "DELETE") == 0 )
            handleDelete(c, hm);
        else 
        {
            piboxLogger(LOG_ERROR, "Unsupported HTTP method: %s\n", method);
            mg_http_reply(c, 401, NULL, "\n");
        }

        free(method);
    }
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 *
 * Notes:
 * This is a RESTful web server based on Mongoose.
 * It's purpose is to provide a web inteface for sensors and Jarvis to
 * communication with Ironman's monitor.
 * ========================================================================
 */
int
main(int argc, char *argv[])
{
    struct mg_mgr   mgr;
    char            server_url[128];
    char            *webroot;
    int             fd;

    /* Load saved configuration and parse command line */
    initConfig();
    parseArgs(argc, argv);

    /* Setup logging */
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    printf("Verbosity level: %d\n", piboxLoggerGetVerbosity());

    if ( cliOptions.logFile != NULL )
    {
        piboxLogger(LOG_INFO, "Log file: %s\n", cliOptions.logFile);
    }
    else
    {
        piboxLogger(LOG_INFO, "No log file configured.\n");
    }

    /* Make sure configured options are okay. */
    if ( validateConfig() != 0 )
    {
        piboxLogger(LOG_ERROR, "Configuration failure.\n");
        goto skipit;
    }

    /* Setup signal handling */
    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);

    /* Initialize server variables. */
    initSetup();

    /* Initialize libcurl. */
    curl_global_init(CURL_GLOBAL_ALL);

    /* Initialize Mongoose. */
    mg_mgr_init(&mgr);        // Init manager
    mg_log_set(MG_LL_INFO);  // Set debug log level. Default is MG_LL_INFO
    sprintf(server_url, "http://0.0.0.0:%d", HTTP_PORT);
    mg_http_listen(&mgr, server_url, frontEnd, NULL);  // Setup listener

    /* Mark server as running. */
    setCLIFlag(CLI_SERVER);

    /* Initialize file system database. Ignore rc from utilsScanFS(). */
    (void)utilsScanFS(NULL);

    /* Start registration thread. */
    piboxLogger(LOG_INFO, "Calling startRegistration.\n");
    startRegistration();

    /* Schedule actual registration. This just gives web interface time to startup. */
    piboxLogger(LOG_INFO, "Scheduling registration.\n");
    mg_timer_add(&mgr, 10000, MG_TIMER_REPEAT, checkRegistration, NULL);
    mg_timer_add(&mgr, 10150, MG_TIMER_REPEAT, timer_genGraph, NULL);

    /* Switch to the webroot */
    webroot = initGetIronmanDir();
    fd = open(webroot, O_PATH);
    if ( fd == -1 )
    {
        piboxLogger(LOG_ERROR, "Failed to switch to webroot: %s\n", webroot);
        goto fail_webroot;
    }
    fchdir(fd);

    /* Spin waiting for connections. */
    piboxLogger(LOG_INFO, "Spinning.\n");
    while ( s_signo == 0 )
        mg_mgr_poll(&mgr, 1000);  

    close(fd);

fail_webroot:
    free(webroot);

    /* Mark server as not running. */
    unsetCLIFlag(CLI_SERVER);

    /* Shutdown registration */
    shutdownRegistration();

    /* Clean up Mongoose. */
    mg_mgr_free(&mgr);

skipit:
    piboxLoggerShutdown();
    return 0;
}
