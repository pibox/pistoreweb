/*******************************************************************************
 * pistoreweb - a UI and RESTful web server based on Mongoose.
 *
 * get.h:  Handlers for GET requests.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef GET_H
#define GET_H

/*========================================================================
 * Defined values
 *=======================================================================*/
/* This aligns with the text in the template file. */
#define LINK_TEMPLATE   "                        <tr><td><a href=\"%s\">%s</a></td></tr>\n"

/* The tag we replace in the index page template file. */
#define LINKS_T         "[--LINKS--]"

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef GET_C
extern void pistoreImages( struct mg_connection *c, struct mg_http_message *hm, char *uri);
extern void pistoreDirList( struct mg_connection *c, struct mg_http_message *hm);
extern void pistoreFrontEnd( struct mg_connection *c, struct mg_http_message *hm, char * );
extern void pistoreIdentify( struct mg_connection *c, char *body );
extern void pistoreStatus( struct mg_connection *c, char *body );
extern void pistoreRegister( struct mg_connection *c, struct mg_http_message *hm );
#endif /* !GET_C */
#endif /* !GET_H */
