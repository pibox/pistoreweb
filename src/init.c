/*******************************************************************************
 * pistoreweb - a UI and RESTful server based on Mongoose
 *
 * init.c:  Various utility methods
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define INIT_C

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <uuid.h>
#include <sys/types.h>

#include "pistoreweb.h"

static struct __pistoreweb_init {
    uuid_t uuid;
    char *ironmanDir;
    char *dataDir;
    char *uuidDir;
} pistoreweb_init;

/*========================================================================
 * Name:   initSetup
 * Prototype:  void initSetup( void )
 *
 * Description:
 * Builds directory and filenames based on runtime configuration.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * N/A
 *
 * Notes:
 * Memory allocated here will persist through the life of the program.
 *========================================================================*/
void
initSetup( void )
{
    piboxLogger(LOG_INFO, "init.setup has been called.\n");
    uuid_generate(pistoreweb_init.uuid);

    /* Set directory and stamp files based on where the webroot has been configured. */
    pistoreweb_init.ironmanDir = (char *)calloc(1,strlen(cliOptions.webroot) + 2);
    sprintf(pistoreweb_init.ironmanDir, "%s", cliOptions.webroot);
    pistoreweb_init.dataDir = (char *)calloc(1,strlen(cliOptions.datadir) + strlen(IRONMANDIR) + 2);
    sprintf(pistoreweb_init.dataDir, "%s%s", cliOptions.datadir, IRONMANDIR);

    pistoreweb_init.uuidDir = (char *)calloc(1,strlen(pistoreweb_init.dataDir) + 2);
    sprintf(pistoreweb_init.uuidDir, "%s", pistoreweb_init.dataDir);

    piboxLogger(LOG_INFO, "Webroot directory : %s\n", cliOptions.webroot);
    piboxLogger(LOG_INFO, "Data directory    : %s\n", cliOptions.webroot);
    piboxLogger(LOG_INFO, "Ironman directory : %s\n", pistoreweb_init.ironmanDir);
    piboxLogger(LOG_INFO, "UUID directory    : %s\n", pistoreweb_init.uuidDir);
}

/*========================================================================
 * Name:   initGetIronmanDir
 * Prototype:  char *initGetIronmanDir( void )
 *
 * Description:
 * Returns a copy of the ironman directory string.  This is the directory used
 * to hold TBD files.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * Character string.  Caller is responsible for freeing the returned pointer.
 *
 * Notes:
 * N/A
 *========================================================================*/
char *
initGetIronmanDir( void )
{
    return( strdup(pistoreweb_init.ironmanDir) );
}

/*========================================================================
 * Name:   initGetUUIDDir
 * Prototype:  char *initGetUUIDDir( void )
 *
 * Description:
 * Returns a copy of the UUID directory string.  This is the directory used
 * to hold the UUID file.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * Character string.  Caller is responsible for freeing the returned pointer.
 *
 * Notes:
 * N/A
 *========================================================================*/
char *
initGetUUIDDir( void )
{
    return ( strdup(pistoreweb_init.uuidDir) );
}
