/*******************************************************************************
 * pistoreweb - a UI and RESTful server based on Mongoose
 *
 * utils.c:  Various utility methods
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define UTILS_C

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <unistd.h>
#include <uuid.h>
#include <arpa/inet.h>
#include <curl/curl.h>
#include <pibox/pibox.h>
#include <mntent.h>
#include <cairo/cairo.h>

#include "pistoreweb.h"

struct response {
    char    *response;
};

/* Local prototypes. */
char *utilsGenUUID( void );

static gint largeFont  = 25;
static gint mediumFont = 15;
static gint smallFont  = 10;

/* piboxd listens on this port */
#define PIBOXD_PORT 13910

/*========================================================================
 *========================================================================
 * PRIVATE FUNCTIONS
 *========================================================================
 *========================================================================*/

/*
 *========================================================================
 * Name:    generateRandomColor
 * Prototype:   void generateRandomColor( PI_COLOR_T *mix )
 *
 * Description:
 * Generate a new color using the golden ratio.
 * Color mix is returned in the provided arguments.
 *
 * Arguments:
 * PI_COLOR_T *mix        Where to store the generated color.
 *
 * Notes:
 * Generated colors are added to a 20/20/20 base so we don't have use
 * pure colors.
 *========================================================================
 */
static void generateRandomColor(PI_COLOR_T *mix)
{
    int red   = rand() % 32;
    int green = rand() % 200;
    int blue  = rand() % 256;

    /* mix the color */
    if (mix != NULL) 
    {
        mix->red   = (red + 20);
        mix->green = (green + 20);
        mix->blue  = (blue + 20);
    }
}

/*========================================================================
 *========================================================================
 * PUBLIC FUNCTIONS
 *========================================================================
 *========================================================================*/

/*========================================================================
 * Name:   genGraph
 * Prototype:  void genGraph( GSList *storeList )
 *
 * Description:
 * Generate a PNG image that graphs the available and used storage
 * of all managed USB drives.
 *
 * Input Arguments:
 * GSList *list     A singly-linked list of strings containing storage 
 *                  information.
 *
 * Notes:
 * The generated PNG is written to a file in the web root.
 *========================================================================*/
void
utilsGenGraph( GSList *storeList )
{
    int                     width, height;
    int                     i, idx, bar_width;
    guint                   bars;
    int                     tickstep, offset, endPoint;
    char                    *str;
    char                    *savePtr;
    char                    *name, *totalStr, *availStr, *bsizeStr;
    unsigned long long      total, avail, bsize;
    unsigned long long      calc;
    char                    *sizetype;
    int                     perc;
    cairo_surface_t         *cst;
    cairo_t                 *cr;
    cairo_text_extents_t    extents;
    char                    buf[PATH_MAX];

    PI_COLOR_T              mix;
    gint                    fontSize = largeFont;
    char                    *filename;

    /* Avoid multiple calls */
    // Use mutex here.

    piboxLogger(LOG_TRACE2, "Entered\n");

    /* Font size should be configurable. */
    fontSize = largeFont;
    width = 1024;
    height = 768;

    piboxLogger(LOG_TRACE2, "Getting cr\n");
    cst = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
    cr = cairo_create(cst);

    // Fill with black background with the default color
    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_rectangle(cr, 0, 0, width, height);
    cairo_fill(cr);

    // Computer width of each bar in the graph.
    if ( storeList != NULL )
    {
        bars = g_slist_length(storeList);
        bar_width = (width - 10) / bars;
    
        cairo_set_line_width(cr, 0.5);
        for (idx=0; idx<g_slist_length(storeList); idx++)
        {
            str = g_strdup(g_slist_nth_data(storeList,idx));
            name     = strtok_r(str, ":", &savePtr);
            totalStr = strtok_r(NULL, ":", &savePtr);
            availStr = strtok_r(NULL, ":", &savePtr);
            bsizeStr = strtok_r(NULL, ":", &savePtr);

            if (( str == NULL   ) ||
                ( name == NULL  ) ||
                ( bsizeStr == NULL ) ||
                ( totalStr == NULL ) ||
                ( availStr == NULL ))
            {
                piboxLogger(LOG_INFO, "Incomplete data store entry: %s\n", g_slist_nth_data(storeList,idx));
                if ( str )
                    g_free(str);
                continue;
            }
    
            total = strtoull(totalStr, NULL, 10);
            avail = strtoull(availStr, NULL, 10);
            bsize = strtoull(bsizeStr, NULL, 10);
            perc  = 100-(int)((float)avail/(float)total*100.0);
            piboxLogger(LOG_TRACE1, "Mount: %s, total=%lu, avail=%lu, size=%lu\n", name, total, avail, bsize);
            piboxLogger(LOG_TRACE1, "Percent used: %d\n", perc);
    
            // Draw the bar.
            generateRandomColor(&mix);
            cairo_set_source_rgb(cr, mix.red/256.0, mix.green/256.0, mix.blue/256.0);
            cairo_rectangle(cr, 10+(idx*bar_width), height-5, bar_width, -(height-10)*((float)perc/100.0));
            cairo_fill(cr);

            /* Write the mount directory, rotated vertically. */
            piboxLogger(LOG_TRACE1, "Writing mount directory name.\n");
            cairo_set_font_size(cr, fontSize);
            cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);

            sprintf(buf, "%s", name);
            cairo_text_extents (cr, buf, &extents);
            cairo_move_to(cr, 5+(idx*bar_width)+(bar_width/2)-extents.height-5, height-10);
            cairo_save(cr);
            cairo_rotate(cr, -1.5708); // 90 degree rotation
            cairo_show_text(cr, buf);
            cairo_restore(cr);

            calc = (unsigned long long)(avail*bsize/GB);
            if ( calc > 0 )
                sizetype=S_GB;
            else
            {
                calc = (unsigned long long)(avail*bsize/MB);
                if ( calc > 0 )
                    sizetype=S_MB;
                else
                {
                    calc = (unsigned long long)(avail*bsize/KB);
                    sizetype=S_KB;
                }
            }
            sprintf(buf, "Available: %llu%s", calc, sizetype);
            piboxLogger(LOG_TRACE1, "%s\n", buf);
            cairo_text_extents (cr, buf, &extents);
            cairo_move_to(cr, 5+(idx*bar_width)+(bar_width/2), height-10);
            cairo_save(cr);
            cairo_rotate(cr, -1.5708); // 90 degree rotation
            cairo_show_text(cr, buf);
            cairo_restore(cr);

            calc = (unsigned long long)(total*bsize/GB);
            if ( calc > 0 )
                sizetype=S_GB;
            else
            {
                calc = (unsigned long long)(total*bsize/MB);
                if ( calc > 0 )
                    sizetype=S_MB;
                else
                {
                    calc = (unsigned long long)(total*bsize/KB);
                    sizetype=S_KB;
                }
            }
            sprintf(buf, "Total: %llu%s", calc, sizetype);
            piboxLogger(LOG_TRACE1, "%s\n", buf);
            cairo_text_extents (cr, buf, &extents);
            cairo_move_to(cr, 5+(idx*bar_width)+(bar_width/2)+extents.height, height-10);
            cairo_save(cr);
            cairo_rotate(cr, -1.5708); // 90 degree rotation
            cairo_show_text(cr, buf);
            cairo_restore(cr);

            if ( str )
                g_free(str);
        }
    }
    else
        piboxLogger(LOG_INFO, "No stores to graph.\n");

    /* overlay axis for the graphs on the mask */
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 0.85);
    cairo_move_to(cr, 10, height-5);
    cairo_line_to(cr, width-10, height-5);
    cairo_stroke(cr);

    cairo_move_to(cr, 10, height-5);
    cairo_line_to(cr, 10, 5);
    cairo_stroke(cr);

    /* Ticks and labels for the graphs */
    endPoint = width-10;
    cairo_set_font_size(cr, smallFont);
    tickstep = (height-10) / 10;
    offset = 0;
    perc=90;
    cairo_set_line_width(cr, 0.5);
    for(i=0; i<10; i++)
    {
        cairo_move_to(cr, 0, offset);
        cairo_line_to(cr, endPoint, offset);
        cairo_stroke(cr);
        offset += tickstep;

        sprintf(buf, "%d%%", perc);
        cairo_move_to(cr, 10, offset-3);
        cairo_save(cr);
        cairo_rotate(cr, -1.5708); // 90 degree rotation
        cairo_show_text(cr, buf);
        cairo_restore(cr);
        perc -= 10;
    }

    /* We're already in the webroot by the time we get here. */
    filename = (char *)calloc(1, strlen(PNGGRAPH) + 2 );
    sprintf(filename, "%s", PNGGRAPH);

    /* Ignore errors when removing the old file, if it doesn't exist. */
    unlink(filename);

    /* Write the PNG file */
    if ( cairo_surface_write_to_png (cst, filename) != CAIRO_STATUS_SUCCESS )
        piboxLogger(LOG_ERROR, "Failed to write PNG.\n");

    /* Cleanup */
    free(filename);

    /* Don't need the cairo objects now */
    cairo_destroy(cr);
    cairo_surface_destroy(cst);
}

/*========================================================================
 * Name:   parse
 * Prototype:  int utilsParse( char *, char **, int )
 *
 * Description:
 * Parse a character string into an array of character tokens.
 *
 * Input Arguments:
 * char *line       The string to parse
 * char **argv      The parsed tokens
 * int  max         Maximum number of arguments to parse
 *
 * Returns:
 * Number of arguments in argv.
 *
 * Note:
 * Borrowed from http://www.csl.mtu.edu/cs4411.ck/www/NOTES/process/fork/shell.c
 *========================================================================*/
int
utilsParse(char *line, char **argv, int max)
{
    int idx = 0;

    /* if not the end of line ....... */
    while (*line != '\0')
    {
        /* replace white spaces with 0 */
        while (*line == ' ' || *line == '\t' || *line == '\n')
            *line++ = '\0';

        /* save the argument position */
        if ( idx < max )
            *argv++ = line;

        /* skip the argument until ... */
        while (*line != '\0' && *line != ' ' && *line != '\t' && *line != '\n')
            line++;

        if ( ++idx == max )
            break;
    }

    /* mark the end of argument list  */
    *argv = '\0';

    return idx;
}

/*========================================================================
 * Name:   utilsLoadUUID
 * Prototype:  char *utilsLoadUUID( )
 *
 * Description:
 * Load the UUID from it's stored file.
 *
 * Returns:
 * A UUID string or NULL if the string can't be retrieved.
 * The caller is required to free the returned string.
 *========================================================================*/
char *
utilsLoadUUID()
{
    char            *srcDir;
    char            *filename;
    char            *uuid = NULL;
    char            *data;
    struct stat     stat_buf;
    FILE            *fd;

    srcDir = initGetUUIDDir();
    piboxLogger(LOG_INFO, "srcDir: %s\n", srcDir);
    filename = (char *)calloc(1, strlen(srcDir) + strlen(UUIDFILE));
    sprintf(filename, "%s/%s", srcDir, UUIDFILE);
    piboxLogger(LOG_INFO, "UUID file: %s\n", filename);
    free(srcDir);

    if ( stat(filename, &stat_buf) == 0 )
    {   
        fd = fopen(filename, "r");
        if ( fd )
        {
            data = (char *)calloc(1, stat_buf.st_size + 1);
            if ( fread(data, 1, stat_buf.st_size, fd) == stat_buf.st_size )
            {
                piboxLogger(LOG_INFO, "File data: %s\n", data);
                uuid = strdup(data);
            }
            else
                piboxLogger(LOG_ERROR, "Failed to read uuid file: %s\n", filename);
            free(data);
            fclose(fd);
        }
        else
            piboxLogger(LOG_ERROR, "Failed to open uuid file: %s\n", filename);
    }
    else
        piboxLogger(LOG_ERROR, "Failed to find uuid file: %s\n", filename);

    free(filename);
    return uuid;
}

/*========================================================================
 * Name:   utilsLocalIP
 * Prototype:  int utilsLocalIP( char * )
 *
 * Description:
 * Test if an IP is from the local host (re: loopback or assigned IP).
 *
 * Input Arguments:
 * char *requestIP    The IP address of the remote device to check.
 *
 * Returns:
 * 0 if the requestIP is not from the local host.
 * 1 if the requestIP is from the local host.
 *========================================================================*/
int
utilsLocalIP(char *requestIP)
{
    int fd;
    int i=0;
    int rc=0;
    struct ifreq ifr;
    char ipaddr[32];
    DIR *pdir = NULL;
    struct dirent *pent = NULL;

    piboxLogger(LOG_INFO, "Checking %s\n", requestIP);
    if ( strcmp(requestIP, LOCALHOST) == 0 )
        return 1;

    pdir = opendir( "/sys/class/net" );
    if (pdir != NULL)
    {
        /* Iterate files */
        while( (pent = readdir(pdir)) != NULL )
        {
            /* Get a socket to use with the ioctl. */
            fd = socket(AF_INET, SOCK_DGRAM, 0);
            memset(&ifr, 0, sizeof(ifr));
            ifr.ifr_addr.sa_family = AF_INET;

            /* Copy the interface name in the ifreq structure */
            memcpy(ifr.ifr_name , pent->d_name , IFNAMSIZ-1);

            /* Issue ioctl to get IP addresses. */
            if ( ioctl(fd, SIOCGIFADDR, &ifr) != 0 )
            {
                continue;
            }

            /* Close the socket. */
            close(fd);

            memset(ipaddr, 0 , 32);
            strcpy(ipaddr, inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr) );
            if ( strcmp(requestIP, ipaddr) == 0 )
            {
                rc=1;
                break;
            }
            i++;
        }
        closedir(pdir);
    }
    return rc;
}

/*========================================================================
 * Name:   utilsBuildResponse
 * Prototype:  void utilsBuildResponse( char *buffer, size_t size, size_t nitems, void *userdata )
 *
 * Description:
 * Inbound data is sent here by curl.  But a single response may not be a complete body of a single response.
 * So this function builds the response over one or more calls from curl.
 *
 * Input Arguments:
 * char *chunk      Partial (or all) return data from an HTTP call to a sensor. This is not NULL terminated.
 * size_t size      Always 1.
 * size_t nitems    Size of the data.
 * void *userdata   Storage for the inbound response.
 *
 * Returns:
 * 0 on failure, size*nitems (total bytes handled) on success.
 *
 * Notes:
 * This is the callback for curl calls with return data from utilsHttpPost().
 *========================================================================*/
static int
utilsBuildResponse( char *chunk, size_t size, size_t nitems, void *userdata )
{
    size_t realsize = size * nitems;
    struct response *data = (struct response *)userdata;
    char *ptr;
 
    if ( !data )
    {
        piboxLogger(LOG_ERROR, "Response data pointer is null.\n");
        return 0;
    }

    if ( data->response != NULL )
        ptr = (char *)calloc(1, strlen(data->response) + realsize + 1);
    else
        ptr = (char *)calloc(1, realsize + 1);

    if(!ptr)
    {
        piboxLogger(LOG_ERROR, "Failed to allocate new response chunk.\n");
        return 0;  /* out of memory! */
    }
 
    if ( data->response )
    {
        strcpy(ptr, data->response);
        memcpy((char *)(ptr+strlen(data->response)), chunk, realsize);
        free(data->response);
    }
    else
    {
        memcpy((char *)ptr, chunk, realsize);
    }
    data->response = ptr;
    return realsize;
}

/*========================================================================
 * Name:   utilsHttpPost
 * Prototype:  void utilsHttpPost( char *dest, char *uri, char *post_data )
 *
 * Description:
 * POST a request to a remote device.
 * Returns the body of the response as a string.
 * Caller is responsible for decrypting responses.
 *
 * Input Arguments:
 * char *dest       Remote device address
 * char *uri        API path
 * char *post_data  Data to send with request, if any.
 *
 * Returns:
 * Character string response or NULL if not response.
 *
 * Notes:
 * Mongoose is a web server, so it doesn't necesasrily make web requests easily.
 * Instead, we use libcurl to make requests.
 *========================================================================*/
void
utilsHttpPost( char *dest, char *uri, char *post_data )
{
    CURL        *curl;
    CURLcode    res;
    char        *url;
    int         port = 80;
    struct response data = {.response = NULL};

    /* In test mode, we let the sensor simulator run on a non-standard port. */
    if ( isCLIFlagSet( CLI_TEST) )
        port = HTTP_PORT_T;

    curl = curl_easy_init();
    if(curl) 
    {
        /* Build the full URL, sans POST data. */
        url = (char *)calloc(1, strlen(dest) + strlen(uri) + 14);
        sprintf(url, "http://%s:%d%s", dest, port, uri);
        piboxLogger(LOG_INFO, "URL: %s\n", url);

        /* 
         * First set the URL that is about to receive our POST. This URL can
         * just as well be an https:// URL if that is what should receive the
         * data.
         */
        curl_easy_setopt(curl, CURLOPT_URL, url);

        /* Now specify the POST data */
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post_data);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, strlen(post_data));
        curl_easy_setopt(curl, CURLOPT_POST, 1L);

        /* Setup for handling any respnose */
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, utilsBuildResponse);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);

        /* Perform the request, res will get the return code */
        res = curl_easy_perform(curl);

        /* Check for errors */
        if(res != CURLE_OK)
            piboxLogger(LOG_ERROR, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));

        /* always cleanup */
        curl_easy_cleanup(curl);

        /* Cleanup */
        free(url);

        /* Handle response, if any. */
        if ( data.response != NULL )
        {
            piboxLogger(LOG_INFO, "Response data: %s\n", data.response);
            // deviceSaveSensorData( data.response, dest );
            free(data.response);
            data.response = NULL;
        }
        else
            piboxLogger(LOG_INFO, "Response data is NULL\n");
    }
    else
        piboxLogger(LOG_ERROR, "Failed to init curl for POST operation.\n");

    return;
}

/*========================================================================
 * Name:   utilsGetRemoteIP
 * Prototype:  char *utilsGetRemoteIP( char * )
 *
 * Description:
 * Convert the remote IP to a string.
 *
 * Input Arguments:
 * char *requestIP    The IP address of the remote device to check.
 *
 * Returns:
 * Remote IP as a quad-formatted character string or NULL if the conversion
 * cannot be done.
 *========================================================================*/
char *
utilsGetRemoteIP( struct mg_connection *c )
{
    char    *buf;

    buf = (char *)calloc(1, 17);
    sprintf(buf, "%d.%d.%d.%d", 
                c->rem.ip[0],
                c->rem.ip[1],
                c->rem.ip[2],
                c->rem.ip[3]);
    return(buf);
}

/*========================================================================
 * Name:   utilsGenUUID
 * Prototype:  char *utilsGenUUID( void )
 *
 * Description:
 * Generate a UUID string.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * A 37 character string.
 *========================================================================*/
char *
utilsGenUUID( void )
{
    static uuid_t   uuid;
    char            *value;

    uuid_generate(uuid);
    value = (char *)calloc(1,37);
    uuid_unparse(uuid, value);
    return(value);
}

/*========================================================================
 * Name:   utilsIdentify
 * Prototype:  char *utilsIdentify( void )
 *
 * Description:
 * Tell appmgr to display the identifier app.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * A 37 character string.
 *========================================================================*/
char *
utilsIdentify( void )
{
    // Send launcher-style message to appmgr to display identifier app.
    return NULL;
}

/*========================================================================
 * Name:   utilsGetStorageTotal
 * Prototype:  char *utilsGetStorageTotal( void )
 *
 * Description:
 * Retrieve total storage configured.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * The size of storage in MB.
 *========================================================================*/
guint
utilsGetStorageTotal( void )
{
    return 0;
}

/*========================================================================
 * Name:   utilsGetStorageAvail
 * Prototype:  char *utilsGetStorageAvail( void )
 *
 * Description:
 * Retrieve total available configured.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * The size of storage in MB.
 *========================================================================*/
guint
utilsGetStorageAvail( void )
{
    return 0;
}

/*========================================================================
 * Name:   utilsGetName
 * Prototype:  char *utilsGetName( void )
 *
 * Description:
 * Retrieve the visual name for this node (not the network name).
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * A character string.  Caller is responsible for freeing the string.
 *========================================================================*/
char *
utilsGetName( void )
{
    return NULL;
}

/*
 *========================================================================
 * Name:    utilsFreeStore
 * Prototype:   void utilsFreeStore( gpointer item )
 *
 * Description:
 * Iterator for freeing up the string for each store.
 *========================================================================
 */
void
utilsFreeStore( gpointer item )
{
    if ( item != NULL )
        free(item);
}

/*========================================================================
 * Name:   scanFS
 * Prototype:  int scanFS( char *path )
 *
 * Description:
 * Scan of mount points.
 *
 * Input Arguments:
 * char *path       If NULL, process all of /proc/mounts.
 *                  If not NULL, process path if found in /proc/mounts.
 *
 * Returns:
 * 0                Scan succeeded for new mount point.
 * 1                Scan failed for requested path.
 *
 * Notes:
 * /proc/mounts SHOULD be updated with path by the time this is called.
 *========================================================================*/
int
utilsScanFS( char *path )
{
    struct mntent   *entry;
    FILE            *fd;
    int             rc = 1;
    char            *mntdir;

    fd = setmntent("/proc/mounts", "r");
    if (fd == NULL)
    {
        piboxLogger(LOG_ERROR, "Can't read /proc/mounts\n");
        return(rc);
    }

    if ( isCLIFlagSet(CLI_TEST) )
    {
        mntdir = MOUNT_DIR_T;
    }
    else
    {
        mntdir = MOUNT_DIR;
    }

    if ( path != NULL )
    {
        if (strncmp(path, mntdir, strlen(mntdir)) != 0 )
        {
            piboxLogger(LOG_ERROR, "Requested path not in %s: %s\n", mntdir, path);
            return(rc);
        }
    }

    /* Iterate over mounted file systems */
    while (NULL != (entry = getmntent(fd)))
    {
        if ( path != NULL )
        {
            piboxLogger(LOG_INFO, "Testing *%s* against *%s*\n", entry->mnt_dir, path);
            if (strcmp(path, entry->mnt_dir) != 0 )
            {
                piboxLogger(LOG_TRACE1, "No match.\n");
                continue;
            }
            piboxLogger(LOG_TRACE1, "They match!\n");
        }

        if (strncmp(entry->mnt_dir, mntdir, strlen(mntdir)) != 0 )
        {
            piboxLogger(LOG_TRACE1, "Not a managed mount point: %s\n", entry->mnt_dir);
            continue;
        }

        // Only process certain types of file systems.
        if( (strcmp(entry->mnt_type, MNTTYPE_IGNORE) == 0) &&
            (strcmp(entry->mnt_type, MNTTYPE_SWAP  ) == 0) )
        {
            piboxLogger(LOG_TRACE1, "Skipping IGNORE/SWAP mount type: %s\n", entry->mnt_dir);
            continue;
        }

        piboxLogger(LOG_INFO, "Adding managed mount point: %s\n", entry->mnt_dir);
        dbUpdateFS(entry);
        rc=0;
    }
    endmntent(fd);
    return(rc);
}
