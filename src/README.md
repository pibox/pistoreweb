The files that make up the source to PiStoreWeb come in three flavors.

The first are files supporting encoded/encrypted messaging between an IronMan monitor and an IoT device such as PiStore.
The files in this first category are as follows.  These files are from external sources and should not need editing.

* aes.c
* base64.c
* crypto.c
* pkcs7_padding.c

The second type of file are supporting files for the Web application. These files or ones similar to them can be found
in nearly all PiBox applications.

* cli.c
* init.c
* msgQueue.c
* pistoreweb.c
* utils.c

The last type of file are those that handle HTTP requests.  These files are specific to this web application.

* mongoose.c
* get.c
* delete.c
* xpost.c

Note that "xpost" cannot be named "post" as this woudl (for unknown reasons) break the build.
