/*******************************************************************************
 * pistoreweb - a UI and RESTful server based on Mongoose
 *
 * db.c:  Functions for reading and maintaining directory stats.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define DB_C

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <glib.h>
#include <sys/inotify.h>
#include <sys/vfs.h>
#include <mntent.h>
#include <blkid.h>
#include <pibox/utils.h>
#include <pibox/parson.h>

#include "pistoreweb.h"

/*
 * Linked lists for db files.
 */
GSList  *fsList = NULL;         // List of filesystems being monitored.
static pthread_mutex_t dbMutex = PTHREAD_MUTEX_INITIALIZER;

/*
 *========================================================================
 * STATIC FUNCTIONS
 *========================================================================
 */

/*
 *========================================================================
 * Name:   findFSByPath
 * Prototype:  int findFSByPath( gconstpointer item, gconstpointer user_data )
 *
 * Description:
 * Find FS entry by mount point directory path.
 *========================================================================
 */
static int
findFSByPath( gconstpointer item, gconstpointer user_data )
{
    PISTORE_FS_T *pfs = (PISTORE_FS_T *)item;
    char *path = (char *)user_data;

    if ( strcmp(pfs->mntdir, path) == 0 )
        return 0;
    else
        return 1;
}

/*
 *========================================================================
 * Name:   getDeviceFromMount
 * Prototype:  char *getDeviceFromMount( char * )
 *
 * Description:
 * Searches /proc/mounts and converts a mount point to it's actual device file.
 *
 * Returns:
 * String name of device or NULL if no matching mount point is found.
 *
 * Notes:
 * Caller must free returned string.
 *========================================================================
 */
static char *
getDeviceFromMount( char *mntdir )
{
    FILE    *fd;
    char    *device;
    char    *mount;
    char    *savePtr;
    char    line[2048];

    /* Open /proc/mounts */
    fd = fopen("/proc/mounts", "r");
    if ( !fd )
        return NULL;

    /* Iterate lines */
    while (fgets(line, 2047, fd) )
    {
        piboxStripNewline(line);
        device = strtok_r(line, " ", &savePtr);
        mount = strtok_r(NULL, " ", &savePtr);
        if ( strcmp(mount, mntdir) == 0 )
        {
            fclose(fd);
            return strdup(device);
        }
    }
    fclose(fd);
    return NULL;
}

/*
 *========================================================================
 * Name:   getLabelFromDevice
 * Prototype:  char *getLabelFromDevice( char * )
 *
 * Description:
 * Retrieve the partition label, if any, from the specified device partition.
 *
 * Returns:
 * String label of device or NULL if no label exists.
 *
 * Notes:
 * Caller must free returned string.
 *========================================================================
 */
static char *
getLabelFromDevice( char *device )
{
    blkid_probe     pr;
    blkid_partlist  ls;
    blkid_partition par;
    char            *name = NULL;
    int             nparts;

    /* Probe the partition associated with the device filename. */
	pr = blkid_new_probe_from_filename(device);
	if (!pr)
    {
		piboxLogger(LOG_ERROR, "%s: failed to create a new blkid probe: %s\n", device, strerror(errno));
        return NULL;
    }

    /* 
     * Get a list of partitions from the probe.  In this case there should only
     * be one since we probed a device filename specific to a single partition.
     */
    ls = blkid_probe_get_partitions(pr);
	if (ls == NULL)
    {
		piboxLogger(LOG_ERROR, "%s: failed to read partitions.\n", device);
        goto getLabelFromDevice_Done;
    }

    /* Verify we have a partition. */
    nparts = blkid_partlist_numof_partitions(ls);
    if (!nparts)
        goto getLabelFromDevice_Done;

    /* 
     * Assume first partition has partition label.
     * This is reasonable since we probed the partition from a specific device filename.
     */
    par = blkid_partlist_get_partition(ls, 0);
    if ( blkid_partition_get_name(par) )
        name = strdup( blkid_partition_get_name(par) );

getLabelFromDevice_Done:
    blkid_free_probe(pr);
    return(name);
}

/*
 *========================================================================
 * Name:   _sumFS
 * Prototype:  void _sumFS( gpointer item, gpointer user_data )
 *
 * Description:
 * Sum storage totals and used for filesystem mounts.
 * 
 * Notes:
 * This requires linking to libblkid.
 * See: https://github.com/util-linux/util-linux/tree/master/libblkid
 *========================================================================
 */
static void
_sumFS( gpointer item, gpointer user_data )
{
    PISTORE_FS_T *  pfs     = (PISTORE_FS_T *)item;
    GSList          **list  = (GSList **)user_data;
    struct statfs   *statfs;
    struct stat     stat_buf;
    char            *mntdir;
    char            buf[1024];
    char            *str;
    char            *device = NULL;
    char            *label = NULL;
    int             flags = 0;

    if ( pfs == NULL )
        return;

    statfs = (struct statfs *)(pfs->statfs);
    mntdir = (char *)pfs->mntdir;
    memset(buf,0,1024);

    /* Test for stamp files on the mntdir */
    sprintf(buf, "%s/%s", mntdir, S_RO_STAMP);
    if ( stat(buf, &stat_buf) == 0 )
        flags |= F_RO_STAMP;
    sprintf(buf, "%s/%s", mntdir, S_PS_STAMP);
    if ( stat(buf, &stat_buf) == 0 )
        flags |= F_PS_STAMP;

    /* Convert mntdir to it's associated device file */
    device = getDeviceFromMount(mntdir);
    if ( device )
        label = getLabelFromDevice(device);

    memset(buf,0,1024);
    snprintf(buf, 1023, "%s:%llu:%llu:%llu:%s:%s:%s", 
            (char *)mntdir,                         /* Mounted filesystem. This is also what we export. */
            (unsigned long long)statfs->f_blocks,   /* Total disk space on store, in blocks. */
            (unsigned long long)statfs->f_bavail,   /* Available disk space on store, in blocks. */
            (unsigned long long)statfs->f_bsize,    /* blocksize. */
            (flags & F_RO_STAMP)?"Y":"N",           /* Consider the mountpoint read only? */
            (flags & F_PS_STAMP)?"Y":"N",           /* Mountpoint reserved for use by a PiSentry system. */
            (label == NULL)?"None":label            /* Partition label. */
            );
    str = (char *)calloc(1,strlen(buf)+1);
    sprintf(str, "%s", buf);
    piboxLogger(LOG_INFO, "Adding str to GSList: %s\n", str);
    *list = g_slist_append(*list, str);

    /* Cleanup */
    if ( label )
        free(label);
    if ( device )
        free(device);
}

/*
 *========================================================================
 * PUBLIC FUNCTIONS
 *========================================================================
 */

/*
 *========================================================================
 * Name:   dbUpdateFS
 * Prototype:  void dbUpdateFS( struct mntent *mntpt )
 *
 * Description:
 * Updates (and adds, if needed) filesystem entry.
 *
 * Input Arguments:
 * struct mntent *mntpt     Structure from getmntent() with mount point directory.
 *========================================================================
 */
void
dbUpdateFS (struct mntent *mntpt)
{
    PISTORE_FS_T    *pistoreFS;
    struct statfs   fs;
    int             rc;
    GSList          *entry = NULL;

    /* Safety check */
    if ( mntpt == NULL )
        return;
    
    /* stat the fs */
    rc = statfs(mntpt->mnt_dir, &fs);
    if ( rc != 0 )
        return;

    /* Skip some mount points we know we don't need to track. */
    if ( (strncmp(mntpt->mnt_dir, "/sys",  4) == 0) ||
         (strncmp(mntpt->mnt_dir, "/proc", 5) == 0) ||
         (strncmp(mntpt->mnt_dir, "/boot", 5) == 0) ||
         (strncmp(mntpt->mnt_dir, "/run",  4) == 0) ||
         (strncmp(mntpt->mnt_dir, "/var",  4) == 0) ||
         (strncmp(mntpt->mnt_dir, "/tmp",  4) == 0) ||
         (strncmp(mntpt->mnt_dir, "/dev",  4) == 0) )
    {
        return;
    }

    pthread_mutex_lock( &dbMutex );

    /* find it's entry */
    entry = g_slist_find_custom(fsList, (gchar *)(mntpt->mnt_dir), (GCompareFunc)findFSByPath);
    if ( entry == NULL )
    {
        /* not found: add entry */
        piboxLogger( LOG_INFO, "Adding mount dir: %s\n", mntpt->mnt_dir);
        pistoreFS = (PISTORE_FS_T *)calloc(1, sizeof(PISTORE_FS_T));
        pistoreFS->statfs = (struct statfs *)calloc(1, sizeof(struct statfs));
        pistoreFS->mntdir = (char *)calloc(1, strlen(mntpt->mnt_dir)+1);
        memcpy((char *)(pistoreFS->mntdir), (char *)mntpt->mnt_dir, strlen(mntpt->mnt_dir));
        fsList = g_slist_append(fsList, pistoreFS);
    }
    else
        pistoreFS = (PISTORE_FS_T *)entry->data;

    /* update entry */
    memcpy((char *)(pistoreFS->statfs), &fs, sizeof(struct statfs));

    pthread_mutex_unlock( &dbMutex );

}

/*
 *========================================================================
 * Name:   dbRemoveFS
 * Prototype:  void dbRemoveFS( char *mntpt )
 *
 * Description:
 * Removes filesystem entry, if it exists.
 *
 * Input Arguments:
 * char *path     Name of directory to find to remove.
 *========================================================================
 */
void
dbRemoveFS (char *path)
{
    PISTORE_FS_T    *pistoreFS;
    GSList          *entry = NULL;

    /* Safety check */
    if ( path == NULL )
        return;
    
    pthread_mutex_lock( &dbMutex );

    /* find it's entry */
    entry = g_slist_find_custom(fsList, (gchar *)(path), (GCompareFunc)findFSByPath);
    if ( entry != NULL )
    {
        piboxLogger( LOG_INFO, "Found entry to delete for %s\n", path);

        /* found: remove entry from list */
        fsList = g_slist_remove_link(fsList, entry);

        /* Now clean up the entry. */
        piboxLogger( LOG_INFO, "Grabbing pistoreFS.\n");
        pistoreFS = (PISTORE_FS_T *)entry->data;

        piboxLogger( LOG_INFO, "Freeing statfs.\n");
        if (pistoreFS->statfs )
            free( pistoreFS->statfs );

        piboxLogger( LOG_INFO, "Removing mount dir.\n");
        if (pistoreFS->mntdir )
            free( pistoreFS->mntdir );

        piboxLogger( LOG_INFO, "Removing pistoreFS.\n");
        free( pistoreFS );

        piboxLogger( LOG_INFO, "Removing entry.\n");
        g_slist_free_1(entry);
    }

    pthread_mutex_unlock( &dbMutex );
}

/*
 *========================================================================
 * Name:   dbGetStores
 * Prototype:  GSList *dbGetStores( void )
 *
 * Description:
 * Builds a list of entries of the format:
 *     name:total:available
 * The receiver is responsible for freeing the list.
 *========================================================================
 */
GSList *
dbGetStores ( void )
{
    GSList *list = NULL;

    pthread_mutex_lock( &dbMutex );
    piboxLogger( LOG_INFO, "fsList size: %d\n", g_slist_length(fsList));
    g_slist_foreach(fsList, _sumFS, &list);
    piboxLogger( LOG_INFO, "return list size: %d\n", g_slist_length(list));
    pthread_mutex_unlock( &dbMutex );

    return list;
}

/*
 *========================================================================
 * Name:   dbInit
 * Prototype:  void dbInit( void )
 *
 * Description:
 * Initializes structures used by the db functions.
 *========================================================================
 */
void
dbInit ( void )
{
    piboxLogger(LOG_INFO, "Stats db initialized.\n");
}
