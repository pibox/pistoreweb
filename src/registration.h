/*******************************************************************************
 * pistoreweb - a UI and RESTful server based on Mongoose
 *
 * registration.c:  Perform Ironman registration requests.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef REGISTRATION_H
#define REGISTRATION_H

#include <pthread.h>

#define REGISTRATION_GROUP_ADDR              "234.1.42.3"
#define REGISTRATION_GROUP_PORT              13911

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Variables
 *=======================================================================*/

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef REGISTRATION_C

extern void startRegistration( void );
extern void shutdownRegistration( void );
extern void scheduleRegistration( void );
extern void checkRegistration (void *data);

#endif /* !REGISTRATION_C */

/*========================================================================
 * Variable definitions
 *=======================================================================*/

#endif /* !REGISTRATION_H */
