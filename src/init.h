/*******************************************************************************
 * pistoreweb - a RESTful web server based on Mongoose.
 *
 * init.h:  Web service initialization
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef INIT_H
#define INIT_H

/*========================================================================
 * Defined values
 *=======================================================================*/

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/

#define PAIRENABLEDSTAMP    "/imnetconfig"
#define IRONMANDIR          "/ironman"
#define UUIDDIR             "/uuid/"
#define UUIDFILE            "uuid"

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef INIT_C
extern void initSetup( void );
extern void initSetDataDir( char *source );
extern char *initGetIronmanDir( void );
extern char *initGetUUIDDir( void );
#endif /* !INIT_C */
#endif /* !INIT_H */
