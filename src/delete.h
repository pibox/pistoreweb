/*******************************************************************************
 * pistoreweb - a UI and RESTful web server based on Mongoose.
 *
 * post.h:  Handlers for POST requests.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef POST_H
#define POST_H

/*========================================================================
 * Defined values
 *=======================================================================*/

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef POST_C
extern void pistoreDeleteFile( struct mg_connection *c, char *body );
extern void pistoreDeleteDir( struct mg_connection *c, char *body );
#endif /* !POST_C */
#endif /* !POST_H */
