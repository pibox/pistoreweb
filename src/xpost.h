/*******************************************************************************
 * pistoreweb - a UI and RESTful web server based on Mongoose.
 *
 * post.h:  Handlers for POST requests.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef XPOST_H
#define XPOST_H

/*========================================================================
 * Defined values
 *=======================================================================*/

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef XPOST_C
extern void pistoreSetNodename( struct mg_connection *c, char *body );
extern void pistoreSetPW( struct mg_connection *c, char *body );
#endif /* !XPOST_C */
#endif /* !XPOST_H */
