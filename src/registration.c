/*******************************************************************************
 * pistoreweb - a UI and RESTful server based on Mongoose
 *
 * registration.c:  Perform Ironman registration requests.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define REGISTRATION_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <string.h>
#include <errno.h>
#include <glib.h>
#include <semaphore.h>
#include <uuid/uuid.h>
#include <pibox/pibox.h>

#include "pistoreweb.h"

static int serverIsRunning = 0;
static pthread_mutex_t registrationMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t registrationThread;
static sem_t registrationSem;
static guint registration_timer=0;

/*========================================================================
 * Name:   isRegistrationRunning
 * Prototype:  int isRegistrationRunning( void )
 *
 * Description:
 * Thread-safe read of serverIsRunning variable.
 *========================================================================*/
static int
isRegistrationRunning( void )
{
    int status;
    pthread_mutex_lock( &registrationMutex );
    status = serverIsRunning;
    pthread_mutex_unlock( &registrationMutex );
    return status;
}

/*========================================================================
 * Name:   setRegistrationRunning
 * Prototype:  int setRegistrationRunning( void )
 *
 * Description:
 * Thread-safe set of serverIsRunning variable.
 *========================================================================*/
static void
setRegistrationRunning( int val )
{
    pthread_mutex_lock( &registrationMutex );
    serverIsRunning = val;
    pthread_mutex_unlock( &registrationMutex );
}

/*
 *========================================================================
 * Name:   checkRegistration
 * Prototype:  void checkRegistration( gpointer data )
 *
 * Description:
 * Test if registration has completed.  If not, poke the registration thread
 * to try to register again.
 *========================================================================
 */
void
checkRegistration (void *data)
{
	char *uuid;

    piboxLogger(LOG_INFO, "Checking if registration is needed.\n");
    pthread_mutex_lock( &registrationMutex );
	uuid = utilsLoadUUID();
	if ( uuid == NULL )
	{
		if ( sem_post(&registrationSem) != 0 )
			piboxLogger(LOG_ERROR, "Failed to post registration semaphore!\n");
        else
            piboxLogger(LOG_INFO, "Posted registration semaphore.\n");
	}
	else
		free(uuid);
    pthread_mutex_unlock( &registrationMutex );

    /* keep the timer running */
    // return(TRUE);
}

/*========================================================================
 * Name:   registration
 * Prototype:  void registration( CLI_T * )
 *
 * Description:
 * Send registration requests to Ironman monitor.
 * Do this only while a registration file does not exit.
 *
 * Input Arguments:
 * void *arg    Unused.
 *
 * Notes:
 * Timeout for a completed registration is 30s.
 *
 * Sending multicast registration request.  The outbound message has the following format.
 * Byte 0:      Message type  (MT_IRONMAN)
 * Byte 1:      Action type (MA_PAIR_IOT)
 * Byte 3-4:    Unused
 * Byte 5-8:    Integer size of payload (0, but payload size is 4 bytes long)
 * If the server accepts those, it responds with an UUID via the web interface.
 *========================================================================*/
static void *
registration( void *arg )
{
    int                 	sd;
	struct ip_mreq          mreq;
    struct ifaddrs          *ifaddr, *ifa;
    struct in_addr          ip_addr;
    int                     family;
    struct sockaddr_in      groupSock;

	char databuf[8] 	= {0};
	int datalen 		= sizeof(databuf);

  	/* Create a datagram socket on which to send. */
  	sd = socket(AF_INET, SOCK_DGRAM, 0);
  	if(sd < 0)
  	{
    	piboxLogger(LOG_ERROR, "Error opening registration socket: %s\n", strerror(errno));
        pthread_exit(NULL);
  	}
    piboxLogger(LOG_INFO, "Opened registration socket.\n");

  	/* Initialize the group sockaddr structure with a */
  	memset((char *) &groupSock, 0, sizeof(groupSock));
  	groupSock.sin_family = AF_INET;
  	groupSock.sin_addr.s_addr = inet_addr(REGISTRATION_GROUP_ADDR);
  	groupSock.sin_port = htons(REGISTRATION_GROUP_PORT);

    /* use setsockopt() to request that available network interfaces join a multicast group */
    piboxLogger(LOG_INFO, "Adding interfaces to multicast group %s:%d\n", REGISTRATION_GROUP_ADDR, REGISTRATION_GROUP_PORT);
    mreq.imr_multiaddr.s_addr=inet_addr(REGISTRATION_GROUP_ADDR);
    if (getifaddrs(&ifaddr) == -1)
    {   
        piboxLogger(LOG_ERROR, "Failed to get network interfaces list: %s\n", strerror(errno));
        close(sd);
        pthread_exit(NULL);
    }

    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
    {   
        /* Skip interfaces that aren't configured */
        if (ifa->ifa_addr == NULL)
        {
            piboxLogger(LOG_TRACE1, "Skipping %s: not configured.\n", ifa->ifa_name);
            continue;
        }

        /* We only use IPv4 currently (home networks don't need IPv6) */
        family = ifa->ifa_addr->sa_family;
        if ( family != AF_INET )
        {
            piboxLogger(LOG_TRACE1, "Skipping %s: not IPv4.\n", ifa->ifa_name);
            continue;
        }

        /* Skip the loopback interface */
        if ( strncmp(ifa->ifa_name, "lo", 2) == 0 )
        {
            piboxLogger(LOG_TRACE1, "Skipping loopback %s.\n", ifa->ifa_name);
            continue;
        }

        /* Get the IPv4 address for the interface. */
        ip_addr = ((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;

        /* Add the interface to the multicast group. */
        mreq.imr_interface.s_addr=ip_addr.s_addr;
        if (setsockopt(sd,IPPROTO_IP,IP_ADD_MEMBERSHIP,&mreq,sizeof(mreq)) < 0)
        {
            piboxLogger(LOG_ERROR, "Multicast failed to join group: %s\n", strerror(errno));
            close(sd);
            pthread_exit(NULL);
        }
        piboxLogger(LOG_INFO, "%s joined multicast group %s:%d\n", ifa->ifa_name, REGISTRATION_GROUP_ADDR, REGISTRATION_GROUP_PORT);
    }
    freeifaddrs(ifaddr);

	/* Setup registration packet - only needs to be done once. */
	databuf[0] = MT_IRONMAN;
	databuf[1] = MA_PAIR_IOT;

    /* Spin, waiting for connections. */
    piboxLogger(LOG_INFO, "Setting registration loop as running.\n");
    setRegistrationRunning(1);
    while( isCLIFlagSet(CLI_SERVER) ) 
    {
        /* Block waiting for someone to ask us to send registration. */
        piboxLogger(LOG_INFO, "Waiting on semaphore to post.\n");
        sem_wait(&registrationSem);
        piboxLogger(LOG_INFO, "Registration semaphore posted.\n");

        /*
         * A posted semaphore is used to break out of this loop.
         * Double check if we're being told to exit.
         */
        if( ! isCLIFlagSet(CLI_SERVER) )
        {
            piboxLogger(LOG_INFO, "CLI_SERVER is not set.\n");
            break;
        }

		/* Send the registration packet.  A response will come via the /register Web API */
		if( sendto(sd, databuf, datalen, 0, (struct sockaddr*)&groupSock, sizeof(groupSock)) < 0 )
  		{
			piboxLogger(LOG_ERROR, "Error sending registration request: %s\n", strerror(errno));
		}
  		else
    		piboxLogger(LOG_INFO, "Sent registration request to Ironman monitor.\n");
    }

    piboxLogger(LOG_INFO, "Registration thread is exiting.\n");
    setRegistrationRunning(0);

    // Call return() instead of pthread_exit so valgrid doesn't report dlopen problems.
    // pthread_exit(NULL);
    return(0);
}

/*========================================================================
 * Name:   startRegistration
 * Prototype:  void startRegistration( void )
 *
 * Description:
 * Setup thread to handle registration with Ironman monitor.
 *========================================================================*/
void
startRegistration( void )
{
    int rc;

    /* Set up the semaphore used to process inbound messages. */
    if (sem_init(&registrationSem, 0, 0) == -1)
    {   
        piboxLogger(LOG_ERROR, "Failed to get registration semaphore: %s\n", strerror(errno));
        return;
    }
    piboxLogger(LOG_INFO, "Initialized registration semaphore.\n");

    /* Create a thread to handle inbound messages. */
    rc = pthread_create(&registrationThread, NULL, &registration, (void *)NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "Failed to create registration thread: %s\n", strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "Started registration thread.\n");
    return;
}

/*========================================================================
 * Name:   shutdownRegistration
 * Prototype:  void shutdownRegistration( void )
 *
 * Description:
 * Shut down registration processing thread.
 *========================================================================*/
void
shutdownRegistration( void )
{
    int timeOut = 1;

    /* Wake sleeping processor, if necessary. */
    if ( sem_post(&registrationSem) != 0 )
        piboxLogger(LOG_ERROR, "Failed to post registrationSem semaphore.\n");

	/* Remove registration timer. */
    // g_source_remove(registration_timer);

	/* Wait for registration thread to exit. */
    while ( isRegistrationRunning() )
    {
        sleep(1);
        timeOut++;
        if (timeOut == 60)
        {
            piboxLogger(LOG_ERROR, "Timed out waiting on registration thread to shut down.\n");
            return;
        }
    }
    pthread_detach(registrationThread);
    piboxLogger(LOG_INFO, "Registration has shut down.\n");
}

/*========================================================================
 * Name:   scheduleRegistration
 * Prototype:  void scheduleRegistration( void )
 *
 * Description:
 * Schedule registration request.
 *========================================================================*/
void
scheduleRegistration( void )
{
	/* Set timer to enable registration check to run every 15 seconds. */
    piboxLogger(LOG_INFO, "Setting up registration timer.\n");
    // registration_timer = g_timeout_add(5000, checkRegistration, NULL);
    piboxLogger(LOG_INFO, "registration timer: %d\n", registration_timer);
}

