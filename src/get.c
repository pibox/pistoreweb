/*******************************************************************************
 * pistoreweb - a UI and RESTful server based on Mongoose
 *
 * get.c:  GET processing for pistoreweb.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define GET_C

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <uuid.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <cairo/cairo.h>

#include "pistoreweb.h"

static pthread_mutex_t getMutex = PTHREAD_MUTEX_INITIALIZER;

/*========================================================================
 *========================================================================
 * STATIC FUNCTIONS
 *========================================================================
 *========================================================================*/

/*========================================================================
 *========================================================================
 * PUBLIC FUNCTIONS
 *========================================================================
 *========================================================================*/

/*========================================================================
 * Name:   pistoreStatus
 * Prototype:  void pistoreStatus( struct mg_connection *c, char *body )
 *
 * Description:
 * Retrieve the status of storage under management. This is a JSON 
 * object with the following fields.
 * total:  int      Total storage of all devices.
 * avail:  int      Available (re: unused) storage of all devices.
 * name:   string   Visual name of pistore node (not it's network name).
 *
 * Input Arguments:
 * struct mg_connections *c     Mongoose's parse HTTP request.
 * char *body                   JSON object from remote node.
 *
 * Returns:
 * An encoded, encrypted JSON object with the described data.
 *
 * Notes:
 * This is an API function so it must unpack, decode and unencrypt the
 * request.
 *
 * The remote node makes the api request
 *      /api/version/status
 *
 * This node responds with a JSON packet of the format
 * {
 *      uuid: <uuid>,
 *      type: "pistore",
 *      description: "pistore",
 *      name: <visual name>,
 *      total_storage: <total storage configured>,
 *      available_storage: <available, re: unused, storage amount>,
 * }
 *========================================================================*/
void
pistoreStatus( struct mg_connection *c, char *body )
{
    char        *message;
    char        *crypt_json;
    char        *response;
    JSON_Value  *update_value;
    JSON_Object *update_object;
    guint       total;
    guint       avail;
    char        *name;
    char        *uuid;

    piboxLogger(LOG_INFO, "Entered.\n");

    message = cryptoDecrypt(body);
    if ( message == NULL )
    {
        piboxLogger(LOG_INFO, "Unauthorized - Failed to decode request.");
        mg_http_reply(c, 401, "", "");
        return;
    }
    if ( strncmp(message, "status", 4) != 0 )
    {
        piboxLogger(LOG_ERROR, "Unauthorized - decoded request doesn't match status request.\n");
        mg_http_reply(c, 401, "", "");
        return;
    }

    piboxLogger(LOG_INFO, "Received valid status request.\n");

    /* 
     * Build the status JSON object and return it to the remote node.
     * The remote node makes the request but expects no answer, and this node
     * responds by calling an API on the remote node to return the data requested.
     */

    /* Build an update packet */
    update_value = json_value_init_object();
    update_object = json_value_get_object(update_value);

    /* Grab the UUID we use for encryption. */
    uuid = utilsLoadUUID();

    /* Retrieve total configured storage. */
    total = utilsGetStorageTotal();

    /* Retrieve total available storage. */
    avail = utilsGetStorageAvail();

    /* Retrieve visual name. */
    name = utilsGetName();

    json_object_set_string( update_object, "uuid", uuid );
    json_object_set_string( update_object, "type", "pistore" );
    json_object_set_string( update_object, "descripotion", "pistore" );
    json_object_set_string( update_object, "name", name );
    json_object_set_number( update_object, "total_storage", total );
    json_object_set_number( update_object, "available_storage", avail );

    free(name);
    free(uuid);

    response = json_serialize_to_string(update_value);
    json_value_free(update_value);
    piboxLogger(LOG_INFO, "Response: %s\n", response);
    crypt_json = cryptoEncrypt(response);
    free(response);
    if ( crypt_json != NULL )
        piboxLogger(LOG_INFO, "%s\n", crypt_json);
    else
    {
        piboxLogger(LOG_ERROR, "Failed to encrypt response\n");
        mg_http_reply(c, 500, "", "");
        return;
    }

    /* Send it to the monitor */
    mg_http_reply(c, 200, "", crypt_json);
    json_free_serialized_string(crypt_json);
}

/*========================================================================
 * Name:   pistoreIdentify
 * Prototype:  void pistoreIdentify( struct mg_connection *c, char *body )
 *
 * Description:
 * Toggle the "identify" feature of the node.  The identify feature allows
 * a user to see which pistore device is associated with a entry on the 
 * Ironman Monitor.
 *
 * Input Arguments:
 * struct mg_connections *c     Mongoose's parse HTTP request.
 * char *body                   JSON object from remote node.
 *
 * Returns:
 * An encoded, encrypted JSON object with the described data.
 *
 * Notes:
 * This is an API function so it must unpack, decode and unencrypt the
 * request.
 *========================================================================*/
void
pistoreIdentify( struct mg_connection *c, char *body )
{
    char    *message;

    piboxLogger(LOG_INFO, "Entered.\n");

    message = cryptoDecrypt(body);
    if ( message == NULL )
    {
        piboxLogger(LOG_INFO, "Unauthorized - Failed to decode request.");
        mg_http_reply(c, 401, "", "");
        return;
    }
    if ( strncmp(message, "identify", 4) != 0 )
    {
        piboxLogger(LOG_ERROR, "Unauthorized - decoded request doesn't match status request.\n");
        mg_http_reply(c, 401, "", "");
        return;
    }

    piboxLogger(LOG_INFO, "Received valid identify request.\n");

    /* Enable the identify feature. */
    utilsIdentify();

    mg_http_reply(c, 200, "", "");
}

/*========================================================================
 * Name:   pistoreFrontEnd
 * Prototype:  void pistoreFrontEnd( struct mg_connection *c, struct mg_http_message *hm, char *uri)
 *
 * Description:
 * Display the status page as HTML.
 *
 * Input Arguments:
 * struct mg_connections *c     Mongoose's parse HTTP request.
 *
 * Notes:
 * This is not an API function.  It ignores the body of the request.
 *
 * We're already in the webroot, so file access is relative to that.
 *========================================================================*/
void
pistoreFrontEnd( struct mg_connection *c, struct mg_http_message *hm, char *uri)
{
    FILE    *fd;
    char    *indexPage;
    char    *template;
    char    *cwd;
    char    *str;
    char    *name, *savePtr, *page;
    char    *ptr, *entry, *entries=NULL;
    int     idx, len;
    int     offset, remainder;
    GSList  *stores;
    struct stat stat_buf;
    struct mg_http_serve_opts opts = {0};

    piboxLogger(LOG_INFO, "Entered.\n");
    cwd = getcwd(NULL, 0);
    piboxLogger(LOG_INFO, "CWD: %s \n", cwd);
    free(cwd);

    /* Check for template file. */
    indexPage = (char *)calloc(1, strlen(INDEXPG) + 2 );
    sprintf(indexPage, "%s", INDEXPG);
    if ( stat(indexPage, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Failed to find %s\n", INDEXPG);
        mg_http_serve_file(c, hm, F_404, &opts);
        free(indexPage);
        return;
    }

    /* Load the template file */
    fd = fopen(indexPage, "r");
    if ( !fd )
    {
        piboxLogger(LOG_ERROR, "Failed to open %s\n", INDEXPG);
        mg_http_serve_file(c, hm, F_404, &opts);
        free(indexPage);
        return;
    }

    template = (char *)calloc(1, stat_buf.st_size + 1);
    if ( fread(template, 1, stat_buf.st_size, fd) != stat_buf.st_size )
    {
        piboxLogger(LOG_ERROR, "Failed to read %s\n", INDEXPG);
        mg_http_serve_file(c, hm, F_404, &opts);
        free(indexPage);
        fclose(fd);
        return;
    }
    fclose(fd);
    // Build a set of HTML list entries.
    len = 0;
    stores = dbGetStores();
    ptr = NULL;
    for (idx=0; idx<g_slist_length(stores); idx++)
    {  
        str = g_strdup(g_slist_nth_data(stores,idx));
        piboxLogger(LOG_INFO, "str: %s\n", str);
        name = strtok_r(str, ":", &savePtr);
        piboxLogger(LOG_INFO, "Allocating entry for: %s\n", name);
        entry = (char *)calloc(1, (strlen(name)*2)  + strlen(LINK_TEMPLATE) + 1);
        sprintf(entry, LINK_TEMPLATE, name, name);
        len += strlen(entry);
        if ( entries == NULL )
        {
            entries = entry;
        }
        else
        {
            piboxLogger(LOG_INFO, "Allocating ptr.\n");
            ptr = (char *)calloc(1, strlen(entries) + strlen(entry) + 1);
            sprintf(ptr, "%s\n%s", entries, entry);
            free(entries);
            entries = ptr;
            free(entry);
        }
        free(str);
    }

    /*
     * Alocate a new page.  Copy in everything up to the tags.
     * Then replace the tags. Then copy in everything after the tags.
     * Currently there is only one tag, but this could be extended
     * for multiple tags.
     */
    piboxLogger(LOG_INFO, "Allocating page.\n");
    page = (char *)calloc(1, strlen(template) + len + 1);
    ptr = strstr(template, LINKS_T);
    if (ptr)
    {
        offset = (int)(ptr-template);
        remainder = strlen( (char *)(template + offset + strlen(LINKS_T)) );
        memcpy(page, template, offset);
        memcpy((char *)(page + offset), entries, len);
        memcpy((char *)(page + offset + len), (char *)(template + offset + strlen(LINKS_T)), remainder);
    }
    else
        piboxLogger(LOG_ERROR, "Missing tags in template file.\n");

    mg_http_reply(c, 200, "", "%s", page);
    free(page);
    free(template);
    free(indexPage);
    free(entries);
    g_slist_free_full(stores, utilsFreeStore);
}

/*========================================================================
 * Name:   pistoreImages
 * Prototype:  void pistoreImages( struct mg_connection *c, struct mg_http_message *hm, char *uri)
 *
 * Description:
 * Serve up images.
 *
 * Input Arguments:
 * struct mg_connections *c     Mongoose's parse HTTP request.
 *
 * Notes:
 * This is not an API function.  It ignores the body of the request.
 *
 * We're already in the webroot, so file access is relative to that.
 *========================================================================*/
void
pistoreImages( struct mg_connection *c, struct mg_http_message *hm, char *uri)
{
    struct stat stat_buf;
    struct mg_http_serve_opts opts = {
            .mime_types = "png=image/png"
            };

    piboxLogger(LOG_INFO, "Entered.\n");

    /* Check that the requested file exists. */
    if ( stat((char *)(uri+1), &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "No such image: %s\n", uri);
        memset(&opts, 0, sizeof(opts));
        mg_http_serve_file(c, hm, F_404, &opts);
        return;
    }
    if ( ! (stat_buf.st_mode & S_IFREG) )
    {
        piboxLogger(LOG_ERROR, "No such image: %s\n", uri);
        memset(&opts, 0, sizeof(opts));
        mg_http_serve_file(c, hm, F_404, &opts);
        return;
    }
    mg_http_serve_file(c, hm, (char *)(uri+1), &opts);
}

/*========================================================================
 * Name:   pistoreDirList
 * Prototype:  void pistoreDirList( struct mg_connection *c, struct mg_http_message *hm)
 *
 * Description:
 * Display a directory listing from one of the storage devices.
 *
 * Input Arguments:
 * struct mg_connections *c     Mongoose's parse HTTP request.
 *
 * Notes:
 * This is not an API function.  It ignores the body of the request.
 *
 * This function needs to display directories and files in the currently
 * selected directory.  There is no editing of these files/directories
 * because the owner of the files will handle that.  This is for viewing
 * of the directory contents only.
 *========================================================================*/
void
pistoreDirList( struct mg_connection *c, struct mg_http_message *hm)
{
    struct mg_http_serve_opts opts = {0};
    piboxLogger(LOG_INFO, "Entered.\n");
    mg_http_serve_dir(c, hm, &opts);
}

/*========================================================================
 * Name:   pistoreRegister
 * Prototype:  void pistoreRegister( struct mg_http_message *hm )
 *
 * Description:
 * Handle incoming IoT registration notice from Monitor.
 *
 * Input Arguments:
 * struct mg_http_message *hm   The structure holding the query part of the URI.
 *
 * Notes:
 * This is an API function.
 *========================================================================*/
void
pistoreRegister( struct mg_connection *c, struct mg_http_message *hm )
{
    struct mg_str var;
    char *buf, *uuid, *uuidDir, *fullpath;
    FILE *fd;

    pthread_mutex_lock( &getMutex );

    piboxLogger(LOG_INFO, "Entered.\n");

    /* Make sure uuid is not already set. */
    uuid = utilsLoadUUID();
    if ( uuid != NULL )
    {
        free(uuid);
        piboxLogger(LOG_INFO, "Already registered - ignoring call.\n");
        mg_http_reply(c, 200, NULL, "");
        pthread_mutex_unlock( &getMutex );
        return;
    }

    buf = (char *)calloc(1, hm->query.len + 1);
    snprintf(buf, hm->query.len, "%s", hm->query.ptr);
    piboxLogger(LOG_INFO, "Query: *%s*\n", buf);
    free(buf);

    /* Extract the uuid from the URI */
    var = mg_http_var(hm->query, mg_str("uuid"));
    if ( var.ptr == NULL )
    {
        mg_http_reply(c, 400, NULL, "");
        pthread_mutex_unlock( &getMutex );
        return;
    }

    /* Write it to the uuid file. */
    buf = (char *)calloc(1, var.len + 1);
    snprintf(buf, var.len, "%s", var.ptr);
    piboxLogger(LOG_INFO, "Received UUID: %s\n", buf);

    uuidDir = initGetUUIDDir();
    fullpath = (char *)calloc(1, strlen(uuidDir) + strlen(UUIDFILE) + 2);
    sprintf(fullpath, "%s/%s", uuidDir, UUIDFILE);
    fd = fopen(fullpath, "w");
    if ( fd != NULL )
    {
        fprintf(fd, "%s", buf);
        fclose(fd);
        piboxLogger(LOG_INFO, "Wrote UUID to file %s\n", fullpath);
    }
    else
    {
        piboxLogger(LOG_INFO, "Failed to write UUID file to %s\n", fullpath);
    }
    free(fullpath);
    free(uuidDir);
    free(buf);

    /* If all is okay, reply with 202 code. See piboxd:registerDevice() */
    mg_http_reply(c, 202, NULL, "");
    pthread_mutex_unlock( &getMutex );
}
