/*******************************************************************************
 * pistoreweb - a UI and RESTful server based on Mongoose
 *
 * delete.c:  DELETE processing for pistoreweb.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define DELETE_C

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <uuid.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "pistoreweb.h"

/*========================================================================
 *========================================================================
 * STATIC FUNCTIONS
 *========================================================================
 *========================================================================*/

/*========================================================================
 *========================================================================
 * PUBLIC FUNCTIONS
 *========================================================================
 *========================================================================*/

/*========================================================================
 * Name:   pistoreDeleteFile
 * Prototype:  void pistoreDeleteFile( struct mg_connection *c, char *body )
 *
 * Description:
 * Delete file.
 *
 * Input Arguments:
 * struct mg_connections *c     Mongoose's parse HTTP request.
 * char *body                   JSON object from remote node.
 *
 * Returns:
 * An encoded, encrypted JSON object with the described data.
 *
 * Notes:
 * This is an API function so it must unpack, decode and unencrypt the
 * request.
 *========================================================================*/
void
pistoreDeleteFile( struct mg_connection *c, char *body )
{
#if 0
    char    *message;
    char    *name;
    guint   total;
    guint   avail;
#endif

    piboxLogger(LOG_INFO, "Entered.\n");

    // TBD

    mg_http_reply(c, 200, "", "");
}

/*========================================================================
 * Name:   pistoreDeleteDir
 * Prototype:  void pistoreDeleteDir( struct mg_connection *c, char *body )
 *
 * Description:
 * Delete a directory.
 *
 * Input Arguments:
 * struct mg_connections *c     Mongoose's parse HTTP request.
 * char *body                   JSON object from remote node.
 *
 * Returns:
 * An encoded, encrypted JSON object with the described data.
 *
 * Notes:
 * This is an API function so it must unpack, decode and unencrypt the
 * request.
 *========================================================================*/
void
pistoreDeleteDir( struct mg_connection *c, char *body )
{
#if 0
    char    *message;
    char    *name;
    guint   total;
    guint   avail;
#endif

    piboxLogger(LOG_INFO, "Entered.\n");

    // TBD

    mg_http_reply(c, 200, "", "");
}
