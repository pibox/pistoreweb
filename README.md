# pistorerest 

pistoreweb is a RESTful Web Server based on Mongoose that provides encrypted messaging between pistore and the Ironman monitor that runs on a PiBox server.  It also serves static HTML for users to browse storage under management.

The pistoreweb API runs as HTTP (not HTTPS) using a JSON packet that contains an encrypted message.  While the JSON packet can be intercepted, the encrypted messages should be difficult to break.  The reason for using HTTP is because some Ironman sensors, such as those based on the ESP01 (re: ESP8266), cannot support HTTPS messaging but can perform simpler encryption manually.  For the small sized messages that are exchanged this is sufficient.  For pistoreweb, this isn't an issue, but using the same mechanism as the ESP01 makes it easier to debug.

# PiStoreWeb REST API

The format for API requests is as follows.

|Method |API                          |From ...                |Description                             |
|-------|-----------------------------|------------------------|----------------------------------------|
|GET    |/api/version/status          |From monitor            |Retrieve total storage under management |
|GET    |/api/version/identify        |From monitor            |Toggle identify mechanism.              |
|POST   |/api/version/nodename        |From monitor            |Set visual nodename for PiStore device. |
|DELETE |/api/version/file</verified> |From monitor            |Delete a file                           |
|DELETE |/api/version/dir</verified>  |From monitor            |Delete a directory                      |

The Ironman monitor can make API requests to delete files and directories.  It does so using the encoded/encrypted
mechanism.  Other applications must proxy these requests through the Ironman monitor.

# PiStoreWeb UI Interface

Browsers can view current status information and browse directories using PiStoreWeb.  However, file management (delete, move, etc.) is not handled here.  This is because PiStore is not a file management system.  It simply provides remote access to mounted drives.  Remote systems access those drives through PiStore and the remote systems are responsible for managing the files and directories they care about.

|Path   |Example                      |From ...                |Description                             |
|-------|-----------------------------|------------------------|----------------------------------------|
|GET    |/<path>                      |From browser            |Get a directory listing, in HTML.       |
|GET    |/                            |From browser            |Generate HTML showing system status     |
