# Unit Test Programs

Files in this directory are used for testing the imrest server.

## monitor

This is a monitor simulator used to issue requests that would normally come from the Ironman monitor.

## unittest.sh

Front end to running tests.  It calls monitor as needed.
