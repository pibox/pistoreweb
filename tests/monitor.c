/*******************************************************************************
 * monitor - Simulate Monitor requests.
 *
 * monitor.c:  program main
 *
 * Terminology
 * ------------------------------------------------------------------------
 * monitor:     Application running on RPi/PiBox used to manage IoT nodes.
 * IoT node:    Remote node for Ironman, in this case, a PiStore system.
 *
 * This simulates a Monitor app sending requests to a PiStore system and
 * handling the return data.  It is used for unit testing the pistoreweb server.
 *
 * Monitor API commands simulated:
 * GET  /api/<version>/status            From monitor             Retrieve total storage under management
 * GET  /api/<version>/identify          From monitor             Toggle identify mechanism.
 * POST /api/<version>/nodename          From monitor or browser  Set visual nodename for PiStore device.
 * POST /api/<version>/pw                From monitor or browser  Set login password.
 * DELETE /api/<version>/file</verified> From monitor or browser  Delete a file
 * DELETE /api/<version>/dir</verified>  From monitor or browser  Delete a directory
 *
 * Notes:
 * 1. This test fakes that the iot registration for the pistore node has completed.
 *
 * License: 0BSD
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define MONITOR_C

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <curl/curl.h>
#include <pibox/pibox.h>
#include <pibox/utils.h>

#include "monitor.h"

static char *monitorUUID = "4d14eafa-bb64-4425-be80-56f733c36904";
static int s_signo = 0;

/*
 *========================================================================
 *========================================================================
 * STATIC FUNCTIONS
 *========================================================================
 *========================================================================
 */

/*========================================================================
 * Name:   signal_handler
 * Prototype:  void signal_handler( int )
 *
 * Description:
 * Handle signals, which just allow exiting.
 *
 * Input Arguments:
 * int signo        Signal to handle.
 *========================================================================*/
static void 
signal_handler(int signo)
{
    s_signo = signo;
}

#if 0

/*
 *========================================================================
 * Name:   apiVersionMatch
 * Prototype:  char *apiVersionMatch( char *apiVersion )
 *
 * Description:
 * Tests if the specified version matches what we support.
 *
 * Returns:
 * 0 if the API version is supported.
 * 1 if the API version is not supported.
 *========================================================================
 */
static int
apiVersionMatch( char *apiVersion )
{
    return (strcmp(apiVersion, VERSTR) == 0)? 0 : 1;
}

/*
 *========================================================================
 * Name:   setDefaultHeaders
 * Prototype:  char *setDefaultHeaders( void )
 *
 * Description:
 * Set common headers for outbound messages.
 *
 * Notes:
 * Uses:
 * mg_http_reply(struct mg_connection *c, int status_code, const char *headers, const char *body_fmt, ...);
 * Where headers is a collection of "name: value\n\r" strings.
 * So this function just builds that string and returns it to the caller.
 *
 * Caller is responsible for freeing the returned string.
 *========================================================================
 */
static char *
setDefaultHeaders( void )
{
    char *buf;

    // Set the Accept-Version header.
    buf = calloc(1,strlen("Accept-Version: ")+strlen(VERSTR)+3);
    sprintf(buf, "Accept-Version: %s\r\n", VERSTR);
    return(buf);
}
#endif

/*
 *========================================================================
 *========================================================================
 * UNIT TESTS
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   getMonitor
 * Prototype:  void getMonitor( void )
 *
 * Description:
 * Send a GET /status message to node and wait for it's response.
 *
 * Receives:
 * TBD
 *
 * Notes:
 * TBD
 *========================================================================
 */
void
getStatus(void)
{
    utilsHttpGet(cliOptions.server, "/status", 1);
}

/*
 *========================================================================
 * Name:   getIdentify
 * Prototype:  void getIdentify( void )
 *
 * Description:
 * Send a GET /identify message to node and check that it is identifying itself.
 *
 * Receives:
 * TBD
 *
 * Notes:
 * TBD
 *========================================================================
 */
void
getIdentify(void)
{
    utilsHttpGet(cliOptions.server, "/identify", 1);
}

/*
 *========================================================================
 * Name:   postSetNodename
 * Prototype:  void postSetNodename(Monitor void )
 *
 * Description:
 * Send a POST /nodename update to the node.
 *
 * Receives:
 * 200 if nodename update is processed.
 * 401 if nodename update is rejected.
 *
 * Notes:
 * TBD
 *========================================================================
 */
void
postSetNodename(void)
{
    JSON_Value      *root_value;
    JSON_Object     *root_object;
    char            *request;
    char            *message;

    /* Build the JSON command object. */
    root_value = json_value_init_object();
    root_object = json_value_get_object(root_value);
    json_object_set_string( root_object, "uuid", monitorUUID );
    json_object_set_string( root_object, "name", "MyNewName" );

    /* Stringify the JSON command object */
    request = json_serialize_to_string(root_value);

    /* Encrypt the JSON command object. */
    message = cryptoEncrypt(monitorUUID, request);
    piboxLogger(LOG_INFO, "Request to send to imrest: %s\n", message);

    /* Send the request. */
    utilsHttpPost(cliOptions.server, "/nodename", message, 1);

    json_free_serialized_string(request);
    json_value_free(root_value);
}

/*
 *========================================================================
 * Name:   postSetPW
 * Prototype:  void postSetPW(Monitor void )
 *
 * Description:
 * Send a POST /pw update to the node.
 *
 * Receives:
 * 200 if pw update is processed.
 * 401 if pw update is rejected.
 *
 * Notes:
 * TBD
 *========================================================================
 */
void
postSetPW(void)
{
    JSON_Value      *root_value;
    JSON_Object     *root_object;
    char            *request;
    char            *message;

    /* Build the JSON command object. */
    root_value = json_value_init_object();
    root_object = json_value_get_object(root_value);
    json_object_set_string( root_object, "uuid", monitorUUID );
    json_object_set_string( root_object, "pw", "MyNewPW" );

    /* Stringify the JSON command object */
    request = json_serialize_to_string(root_value);

    /* Encrypt the JSON command object. */
    message = cryptoEncrypt(monitorUUID, request);
    piboxLogger(LOG_INFO, "Request to send to imrest: %s\n", message);

    /* Send the request. */
    utilsHttpPost(cliOptions.server, "/pw", message, 1);

    json_free_serialized_string(request);
    json_value_free(root_value);
}

/*
 *========================================================================
 * Name:   deleteFile
 * Prototype:  void deleteFile( void )
 *
 * Description:
 * Send a DELETE /file message to node and wait for it's response.
 *
 * Receives:
 * TBD
 *
 * Notes:
 * TBD
 *========================================================================
 */
void
deleteFile( void )
{
    char            *uuid = NULL;
    char            *message;
    char            *request;
    JSON_Value      *root_value;
    JSON_Object     *root_object;

    /* Build the JSON command object. */
    root_value = json_value_init_object();
    root_object = json_value_get_object(root_value);
    json_object_set_string( root_object, "uuid", uuid );
    json_object_set_string( root_object, "file", "/mainpath/testfile" );

    /* Stringify the JSON command object */
    request = json_serialize_to_string(root_value);

    /* Encrypt the JSON command object. */
    message = cryptoEncrypt(monitorUUID, request);
    piboxLogger(LOG_INFO, "Request to send to imrest: %s\n", message);

    /* Send the request. */
    utilsHttpDelete(cliOptions.server, "/file", message);

    free(message);
    json_free_serialized_string(request);
    json_value_free(root_value);
    free(uuid);
}

/*
 *========================================================================
 * Name:   deleteDirectory
 * Prototype:  void deleteDirectory( void )
 *
 * Description:
 * Send a DELETE /dir message to node and wait for it's response.
 *
 * Receives:
 * TBD
 *
 * Notes:
 * TBD
 *========================================================================
 */
void
deleteDirectory( void )
{
    char            *uuid=NULL;
    char            *message;
    char            *request;
    JSON_Value      *root_value;
    JSON_Object     *root_object;

    /* Build the JSON command object. */
    root_value = json_value_init_object();
    root_object = json_value_get_object(root_value);
    json_object_set_string( root_object, "uuid", uuid );
    json_object_set_string( root_object, "dir", "/mainpath/testdir" );

    /* Stringify the JSON command object */
    request = json_serialize_to_string(root_value);

    /* Encrypt the JSON command object. */
    message = cryptoEncrypt(monitorUUID, request);
    piboxLogger(LOG_INFO, "Request to send to imrest: %s\n", message);

    /* Send the request. */
    utilsHttpDelete(cliOptions.server, "/dir", message);

    free(message);
    json_free_serialized_string(request);
    json_value_free(root_value);
    free(uuid);
}

/*
 *========================================================================
 *========================================================================
 * MAIN
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   main
 * Prototype:  int main( int argc, char **argv )
 *
 * Description:
 * Set common headers for outbound messages.
 *
 * Notes:
 * Uses:
 * mg_http_reply(struct mg_connection *c, int status_code, const char *headers, const char *body_fmt, ...);
 * Where headers is a collection of "name: value\n\r" strings.
 * So this function just builds that string and returns it to the caller.
 *
 * Caller is responsible for freeing the returned string.
 *========================================================================
 */
int
main(int argc, char *argv[])
{
    /* Load saved configuration and parse command line */
    initConfig();
    parseArgs(argc, argv);

    /* Setup logging */
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    printf("Verbosity level: %d\n", piboxLoggerGetVerbosity());

    if ( cliOptions.logFile != NULL )
    {
        piboxLogger(LOG_INFO, "Log file: %s\n", cliOptions.logFile);
    }
    else
    {
        piboxLogger(LOG_INFO, "No log file configured.\n");
    }

    /* Make sure configured options are okay. */
    if ( validateConfig() != 0 )
    {
        piboxLogger(LOG_ERROR, "Configuration failure.\n");
        goto skipit;
    }

    /* Setup signal handling */
    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);

    /* Initialize libcurl. */
    curl_global_init(CURL_GLOBAL_ALL);

    /* Handle test request. */
    switch (cliOptions.test )
    {
        case 1: getStatus();                /* Monitor: GET /status */
            break;

        case 2: getIdentify();              /* Monitor: GET /identify */
            break;

        case 3: postSetNodename();          /* Monitor: POST /nodename */
            break;

        case 4: postSetPW();                /* Monitor: POST /pw */
            break;

        case 5: deleteFile();               /* Monitor: DELETE /file */
            break;

        case 6: deleteDirectory();          /* Monitor: DELETE /dir */
            break;

    }

skipit:
    piboxLoggerShutdown();
    return 0;
}
