/*******************************************************************************
 * monitor - Simulate Monitor requests.
 *
 * monitor.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef MONITOR_H
#define MONITOR_H

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef MONITOR_C
#endif /* MONITOR_C */

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "monitor"
#define MAXBUF      4096
#define MONITOR_T   1

#define TESTETC     "data/etc"
#define IOTDIR      "ironman"

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef MONITOR_C
#endif

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include <pibox/parson.h>
#include "cli.h"
#include "base64.h"
#include "aes.h"
#include "pkcs7_padding.h"
#include "crypto.h"
#include "utils.h"
#include "init.h"

#endif /* !MONITOR_H */
