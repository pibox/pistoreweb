/*******************************************************************************
 * sensor - a RESTful web server that simulates an Arduino sensor node.
 *
 * sensor.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef SENSOR_H
#define SENSOR_H

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef SENSOR_C
#endif /* SENSOR_C */

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "sensor"
#define MAXBUF      4096
#define HTTP_PORT   8166        // Sensors listen on 80, but for testing we use a different port.

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef SENSOR_C
#endif

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include <pibox/parson.h>
#include "mongoose.h"
#include "cli.h"
#include "base64.h"
#include "aes.h"
#include "pkcs7_padding.h"
#include "crypto.h"

#endif /* !SENSOR_H */
