#!/usr/bin/env bash
#--------------------------------------------------------------
# Initialization
#--------------------------------------------------------------
IV="$(uuidgen | sed 's/-//g' | cut -c1-16)"
UUID="105516fb-b8ec-4393-a820-f7b5aa227449"
KEY="$(echo "${UUID}" | cut -c1-16)"

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------

# Provide command line usage assistance
doHelp()
{
    echo ""
    echo "$0 -t testnum"
    echo "where"
    echo "-t testnum  One of:"
    echo "                1: Jarvis GET /monitor"
    echo "                2: Jarvis POST /pair/jarvis"
    echo "                3: Jarvis POST /devices"
    echo "                4: Jarvis POST /set/device"
    echo "                5: Jarvis DELETE /set/device"
    echo "                6: Monitor GET /query/devices"
    echo "                7: Monitor DELETE /set/device"
    echo "                8: IoT POST /pair/iot"
    echo ""
}

getMonitor()
{
    ./tests/jarvis -v3 -s 192.168.101.4 -t1
}

postPairJarvis()
{
    touch data/etc/imnetconfig
    ./tests/jarvis -v3 -s 192.168.101.4 -t2
    rm -f data/etc/imnetconfig

    # Need:
    # Set UUID and verify
    # Set NULL UUID and verify
}

postDevices()
{
    ./tests/jarvis -v3 -s 192.168.101.4 -t3
}

postSetDevice()
{
    ./tests/jarvis -v3 -s 192.168.101.4 -t4
}

deleteSetDevice()
{
    [[ "${1}" == "jarvis" ]] && ./tests/jarvis -v3 -s 192.168.101.4 -t5
    [[ "${1}" == "monitor" ]] && ./tests/jarvis -v3 -s 192.168.101.4 -t7
}

getQueryDevices()
{
    ./tests/jarvis -v3 -s 192.168.101.4 -t6
}

postPairIot()
{
    touch data/etc/imnetconfig
    ./tests/jarvis -v3 -s 192.168.101.4 -t8
    rm -f data/etc/imnetconfig
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":t:" Option
do
    case $Option in
    t) TEST=${OPTARG};;
    *) doHelp; exit 0;;
    esac
done

#--------------------------------------------------------------
# Main
#--------------------------------------------------------------
case ${TEST} in
    1)  getMonitor;;
    2)  postPairJarvis;;
    3)  postDevices;;
    4)  postSetDevice;;
    5)  deleteSetDevice jarvis;;
    6)  getQueryDevices;;
    7)  deleteSetDevice monitor;;
    8)  postPairIot;;
    *) doHelp;;
esac
