/*******************************************************************************
 * pistoreweb - a RESTful web server based on Mongoose.
 *
 * pistoreweb.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PISTOREWEB_H
#define PISTOREWEB_H

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef PISTOREWEB_C
#endif /* PISTOREWEB_C */

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "pistoreweb"
#define MAXBUF      4096
#define HTTP_PORT   80
#define HTTP_PORT_T 80
#define API_VERSION "1.0"

// Where the config file is located
#define F_CFG   "/etc/pistoreweb.cfg"
#define F_CFG_T "data/pistoreweb.cfg"

#define PAIR_IOT     "/pair/iot"
#define PAIR_JARVIS  "/pair/jarvis"

/* Enable AES 128 CBC encryption */
#define CBC 1

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef PISTOREWEB_C
#endif

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include <pibox/parson.h>
#include "mongoose.h"
#include "cli.h"
#include "utils.h"
#include "init.h"
#include "base64.h"
#include "aes.h"
#include "pkcs7_padding.h"
#include "crypto.h"

#endif /* !PISTOREWEB_H */
